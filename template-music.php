<?php
/*
Template Name: Musikschulen
*/
?>

<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<div class="page-header">
					<h1><?php the_title(); ?></h1>
				</div>
				<?php
					/**
					 * Fetch all categories and loop related posts
					 */
					$args = array(
						'type' 			=> 'musicschool',
						'order'			=> 'ASC',
						'taxonomy'  => 'musicschool_category'
					);

					$categories = get_categories($args);
				?>
				<?php foreach($categories as $key => $category ):?>
					<article <?php post_class('accordion musicschool'); ?>>
						<header>
							<h2 class="entry-title"><a href="#"><?php echo $category->name; ?></a></h2>
						</header>
						<div class="entry">
							<?php
								$posts = query_posts(array(
									'post_type' => 'musicschool',
									'showposts' => -1,
									'tax_query' => array(
										array(
											'taxonomy' => 'musicschool_category',
											'terms' => $category->term_id,
											'field' => 'term_id',
										)
									)
								));
							?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="article-row">
									<div class="description">
										<h2><?php the_title(); ?></h2>
										<p><?php the_content(); ?></p>
										<hr>
									</div>

								</div>
							<?php endwhile; ?>
						</div>
					</article>
				<?php endforeach; ?>
				<?php wp_reset_query(); ?>
			</div>
				<?php the_content(); ?>
		</div>
	</div>


</section>
