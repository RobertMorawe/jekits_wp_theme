<section class="main-content">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <?php get_template_part('templates/content', 'single'); ?>
                <!--                <br/><br/><br/>-->
                <?php get_template_part('templates/accordion'); ?>
                <br/><br/><br/>
                <?php
                $form_page_id = get_field('form_page_id');

                if ($form_page_id):
                    ?>
                    <div class="button-wrapper">
                        <?php if (get_field('hide_confirmation_page')): ?>
                                <?php
                                $page_id_download = get_field('page_id_download', 'options');
                                ?>
                                <input checked style="visibility: hidden;" id="agree_yes" type="radio" name="agree[]"
                                       value="yes"/>
                                <a id="agree" class="button"
                                   href="#"><?php echo __('Weiter zur Online-Anmeldung', 'roots'); ?></a>
                                <script>
                                    var agree_links = {
                                        'form': '<?php echo get_permalink($form_page_id); ?>?kurs_id=<?php echo urlencode(get_the_title()); ?>&kurs_name=<?php echo urlencode(get_the_excerpt()); ?>',
                                        'download': "<?php echo get_permalink($page_id_download); ?>"
                                    }
                                </script>
                        <?php else: ?>
                            <a class="button" href="<?php echo get_permalink(); ?>bestaetigung">
                                <?php echo __('Online-Anmeldung', 'roots'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
