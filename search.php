<section class="search">
	<div class="search-bar">
		<div class="content-intro">
			<?php if (!have_posts()) : ?>
			<h2>Suchergebnisse</h2>
			<?php else: ?>
			<h2>Suchergebnisse für "<?php echo get_search_query(); ?>"</h2>
			<?php endif; ?>
			<?php get_search_form(); ?>
		</div>
	</div>
	<div class="container">
		<div class="search-wrap">
			<div class="search-box">
				<?php if (!have_posts()) : ?>
				<h3><?php _e('Leider nichts gefunden.', 'roots'); ?></h3>
				<?php endif; ?>

				<?php while (have_posts()) : the_post(); ?>
					<?php
						$post_type = get_post_type( get_the_ID() );
					?>
					<article class="search-list">
						<header>
							<h3 class="headline"><?php echo get_post_type_object($post_type)->labels->name; ?></h3>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						</header>
						<div class="entry-summary">
							<?php if ( $post_type !== 'schule'): ?>
								<a href="<?php the_permalink(); ?>">
								<?php the_excerpt(); ?>
								</a>
							<?php endif; ?>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
