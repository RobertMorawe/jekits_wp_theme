<section class="content-header" style="background-color: #03256a;">
    <div class="container">
        <div class="content-intro">
            <h1>
                <?php echo roots_title(); ?>
            </h1>
            <h3 class="center"><?php echo get_field('subtitle'); ?></h3>
            <br>
        </div>
    </div>
</section>