<?php
	$color_hex = get_sub_field( 'farbe' );
?>
<section class="flex-content-box" style="text-align: center; background-color: #<?php echo $color_hex; ?>;">
	<div class="container">
		<?php if(get_sub_field('layout') == "col2"): ?>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<?php flex_content_elements('column_1'); ?>
				</div>
				<div class="col-xs-12 col-md-6">
					<?php flex_content_elements('column_2'); ?>
				</div>
			</div>
		<?php else: ?>
			<div class="row">
				<div class="col-xs-12">
					<?php flex_content_elements('column_1'); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>