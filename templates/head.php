<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>
	<?php if ( WP_ENV != 'development' ): ?>
        <!-- Copyright (c) 2000-2015 etracker GmbH. All rights reserved. -->
        <!-- This material may not be reproduced, displayed, modified or distributed -->
        <!-- without the express prior written permission of the copyright holder. -->
        <!-- etracker tracklet 4.0 -->
        <script id="_etLoader" type="text/javascript" charset="UTF-8" data-secure-code="DBVQns"
                src="//static.etracker.com/code/e.js"></script>
        <noscript>
            <link rel="stylesheet" media="all"
                  href="//www.etracker.de/cnt_css.php?et=DBVQns&amp;v=4.0&amp;java=n&amp;et_easy=0&amp;et_pagename=&amp;et_areas=&amp;et_ilevel=0&amp;et_target=,0,0,0&amp;et_lpage=0&amp;et_trig=0&amp;et_se=0&amp;et_cust=0&amp;et_basket=&amp;et_url=&amp;et_tag=&amp;et_sub=&amp;et_organisation=&amp;et_demographic="/>
        </noscript>
	<?php endif; ?>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <!-- etracker tracklet 4.0 end -->
    <link rel='stylesheet' id='roots_css-css' href='<?php echo get_template_directory_uri(); ?>/assets/css/custom.css'
          type='text/css' media='all'/>
</head>
