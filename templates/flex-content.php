<?php while (the_flexible_field("content")):
    switch (get_row_layout()) {
        case "callout-info":
            get_template_part('templates/flex-content/callout-info');
            break;
        case "teaser-row":
            get_template_part('templates/flex-content/teaser-row');
            break;
        case "teaser-row2":
            get_template_part('templates/flex-content/teaser-row2');
            break;
        case "current-news":
            get_template_part('templates/flex-content/current-news');
            break;
        case 'neighborhood-search':
        case "neighbourhood-search":
            get_template_part('templates/neighbourhood-search');
            break;
        case "newsletter":
            get_template_part('templates/flex-content/newsletter');
            break;
        case "gallery":
            get_template_part('templates/flex-content/gallery');
            break;
        case "introbox":
            get_template_part('templates/flex-content/introbox_flex');
            break;
        case "text_and_link_list":
            get_template_part('templates/flex-content/text_and_link_list');
            break;
        case "neighbourhood-search-cluster-map":
            get_template_part('templates/flex-content/cluster-map');
            break;
        case "numbers_visualization":
            get_template_part('templates/flex-content/numbers_visualization');
            break;
        default:
    }
endwhile; ?>