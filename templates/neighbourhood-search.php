<?php
$post_url = get_post_type_archive_link(''); ?>
<section id="jekits-map" class="info-callout light neighbourhood">
    <div class="container">
<<<<<<< HEAD
        <h2>JeKits in meiner Nähe</h2>
        <p class="description">
            Hier sind alle Städte, Grund- und Förderschulen sowie alle außerschulischen Kooperationspartner
            (Musikschulen oder Tanzinstitutionen), die an JeKits teilnehmen, abrufbar.</p>
        <p class="description">
            Suchen Sie z. B. eine Grundschule in Ihrer Nähe, die JeKits anbietet, so geben Sie einfach Ihre Postleitzahl
            in das dafür vorgesehen Feld ein. Oder geben Sie die Stadt, in der Sie suchen, in das rechte Feld ein. Dann
            werden alle Schulen sowie die außerschulischen Kooperationspartner, die JeKits an den Grundschulen
            durchführen, angezeigt. Wenn Sie wissen möchten, ob eine bestimmte Schule JeKits anbietet, so geben Sie
            einfach den Schulnamen in das rechte Feld ein. Nimmt die Schule am JeKits-Programm teil, so wird sie
            angezeigt.
        </p>
        <!--<p class="description">-->
        <!--			Das JeKi-Programm wird bis zum Schuljahr 2017/18 parallel im Ruhrgebiet auslaufen. Alle Städte und Schulen, die noch an JeKi teilnehmen, sind hier abrufbar.-->
        <!--		</p>-->
        <form method="post" action="<?php echo get_posttype_slug(); ?>">

            <!--			<div class="radio-tab">-->
            <input type="hidden" name="type" value="jekits">
            <!--				<div class="jekits radio-wrapper">-->
            <!--                    <input --><?php //if(isChecked() === 'jekits'): ?><!-- checked -->
            <?php //endif; ?><!-- type="radio" name="type" value="jekits">-->
            <!--                    <label for="type">JeKits</label>-->
            <!--				</div>-->
            <!--				<div class="jeki radio-wrapper">-->
            <!--					<input --><?php //if(isChecked() === 'jeki'): ?><!-- checked -->
            <?php //endif; ?><!-- type="radio" name="type" value="jeki">-->
            <!--					<label for="type">JeKi</label>-->
            <!--				</div>-->
            <!--			</div>-->

            <input class="zip" type="text" name="zip" placeholder="Postleitzahl"
                   value="<?php echo get_current_zip(); ?>"/>
            <input class="name_place" type="text" name="term" placeholder="Ort oder Name der Schule"
                   value="<?php echo get_current_term(); ?>"/>
            <input class="hide" type="submit" value="Schulen suchen">

            <p class="data-privacy-text">
                Durch Anklicken der unten stehenden Checkbox und das Anklicken der Schaltfläche
                „Anfrage absenden“ erklären Sie sich damit einverstanden, dass die hier eingegebenen
                Daten zusammen mit ihrer IP-Adresse verarbeitet und an den Anbieter des Kartendienstes
                (Google USA) weiterleiten. Die Verarbeitung erfolgt ausschließlich für den Zweck der
                Nutzung des Angebots „JeKits in meiner Nähe“. Wenn Sie hiermit nicht einverstanden sind,
                ist eine Nutzung der Funktion „JeKits in meiner Nähe“ leider nicht möglich.
            </p
                <p>
                    <input required type="checkbox" id="search" name="google-search" value="search">
                    <label class="plz-search" for="google-search">Ich bin damit einverstanden.</label>
                </p>

        </form>
=======
        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <h2>JeKits in meiner Nähe</h2>
            </div>
            <form method="post" class="" action="<?php echo get_posttype_slug(); ?>">
                <input type="hidden" name="type" value="jekits">
                <div class="col-sm-2">
                    <input class="zip" type="text" name="zip" placeholder="Postleitzahl" value="<?php echo get_current_zip(); ?>"/>
                </div>
                <div class="col-sm-3">
                    <input class="name_place" type="text" name="term" placeholder="Ort oder Name der Schule" value="<?php echo get_current_term(); ?>"/>
                </div>
                <div class="col-sm-2">
                    <input type="submit" value="Schulen suchen">
                </div>
                <br>
                <br>
            </form>
            <div class="col-xs-12 col-sm-5">
                <br>
                <p class="small" style="text-align:left;"><strong>Hier sind alle Städte, Grund- und Förderschulen sowie alle außerschulischen Bildungspartner (Musikschulen oder Tanzinstitutionen), die an JeKits teilnehmen, abrufbar.</strong></p>
            </div>
            <div class="col-xs-12 col-sm-7">
                <br>
                <p class="data-privacy-text small" style="color:white;text-align:left;">
                    Durch Anklicken der unten stehenden Checkbox und das Anklicken der Schaltfläche
                    „Anfrage absenden“ erklären Sie sich damit einverstanden, dass die hier eingegebenen
                    Daten zusammen mit ihrer IP-Adresse verarbeitet und an den Anbieter des Kartendienstes
                    (Google USA) weiterleiten. Die Verarbeitung erfolgt ausschließlich für den Zweck der
                    Nutzung des Angebots „JeKits in meiner Nähe“. Wenn Sie hiermit nicht einverstanden sind,
                    ist eine Nutzung der Funktion „JeKits in meiner Nähe“ leider nicht möglich.
                </p>
                <div class="agree_terms">
                    <div class="checkbox">
                        <label>
                            <input id="terms_agreed" name="terms_agreed" value="1" type="checkbox">
                            Ich bin damit einverstanden.
                        </label>
                    </div>
                </div>
            </div>
        </div>
>>>>>>> origin/relaunch2018
    </div>
</section>


