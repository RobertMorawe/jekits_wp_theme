<section class="main-content school-map">
    <?php
    $zip_query = $term_query = $meta_query = $coordinates = array();
    global $wp_query;

    wp_reset_postdata();

    if ( get_current_zip() !== '' ) {
        $zip_query = array(
            'key' => 'zip',
            'value' => get_current_zip(),
            'compare' => 'LIKE'
        );
    }

    if ( get_current_term() !== '' ) {
        $term_query = array(
            'key' => 'city',
            'value' => get_current_term(),
            'compare' => 'LIKE'
        );
    }

    if ( get_current_term() !== '' && get_current_zip() !== '') {
        $meta_query = array(
            'relation' => 'AND',
            $zip_query,
            $term_query
        );
    }
    else if ( get_current_term() !== '' ) {
        $meta_query = array(
            $term_query
        );
    }
    else {
        $meta_query = array(
            $zip_query
        );
    }

    $search_term = get_current_term();


    $query = new WP_Query(array(
        'post_type' => 'jekits_school',
        'post_status' => 'publish',
        'numberposts' => -1,
        'posts_per_page' => -1,
        'post_title_like' => $search_term,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => $meta_query
    ));

    if ( empty($search_term) ) {
        $search_term = get_current_zip();
    }
    else if (get_current_term() !== '' && get_current_zip() !== '') {
        $search_term = get_current_zip() . ', ' . get_current_term();
    }
    ?>
    <div class="school-list">
        <h2>Suchergebnisse für "<?php echo $search_term; ?>"</h2>
        <?php if ( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <article class="school">
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                    <p><?php the_field('street'); ?> <?php the_field('street_number'); ?><br /><?php the_field('zip'); ?> <?php the_field('city'); ?><br />
                        <a href="http://<?php the_field('webseite'); ?>"><?php the_field('webseite'); ?></a></p>
                </article>
                <?php
                $coordinate = array();
                $acf_map = get_field('map');

                if ( !empty($acf_map) ) {
                    $coordinate = get_field('map');
                    $coordinate['type'] = 'latlng';
                }
                else {
                    $coordinate['type'] = 'address';
                }

                $coordinate['marker'] = 'DF3505';

                if ( get_field('typ') === 'musicschool' ) {
                    $coordinate['marker'] = '00A6EB';
                }

                $coordinate['name'] = get_the_title();
                $coordinate['address'] = get_field('street') . ' ' . get_field('street_number') . ', ' . get_field('zip') . ' ' . get_field('city');

                array_push($coordinates, $coordinate);
                ?>
            <?php endwhile; ?>
        <?php else : ?>
            <p><?php _e( 'Leider konnten wir keine passenden Schulen finden.' ); ?></p>
        <?php endif; ?>
    </div>
    <div class="map">
        <div id="map" style="width: 100%; height: 100%;"></div>
        <?php if (!empty($coordinates)): ?>
            <script>
                var schools = <?php echo json_encode($coordinates); ?>;
            </script>
        <?php endif; ?>
    </div>
</section>
<?php wp_reset_query(); ?>
