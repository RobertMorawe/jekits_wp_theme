<?php
	$accordion = get_field('accordion_fields');

?>
<?php if($accordion): ?>
	<div class="content-accordion">
	<?php foreach ($accordion as $key => $item):
        ?>
		<article <?php post_class('accordion musicschool'); ?> id="<?=(!empty($item['html_id'])?$item['html_id']:sanitize_title($item['title']))?>">
			<header>
				<h2 class="entry-title"><a href="#"><?php echo $item['title']; ?></a></h2>
			</header>
			<div class="entry">
				<?php echo $item['content']; ?>
			</div>
		</article>
	<?php endforeach; ?>
	</div>
<?php endif; ?>