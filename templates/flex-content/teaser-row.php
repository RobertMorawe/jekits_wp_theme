<section class="teaser-row">
        <div class="flex-row">

            <div class="item col-xs-12 col-sm-4">
                <a href="https://intranet.jekits.de/login" class="text" target="_blank">
                    <h2>Intranet für Programmpartner</h2>
                </a>
            </div>
            <div class="item col-xs-12 col-sm-4">
                <a href="#jekits-map" class="text">
                    <h2>Infos für Eltern - JeKits in meiner Nähe</h2>
                </a>
            </div>
            <div class="item col-xs-12 col-sm-4">
                <a href="/fuer-lehrkraefte#material" class="text">
                    <h2>Materialpool für Lehrkräfte</h2>
                </a>
            </div>
        </div>
</section>
