<section class="info-callout white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="intro-box">
                    <h2><?= get_sub_field("title", false) ?></h2>
                    <div class="description dark">
                        <?= get_sub_field("text", true) ?>
                    </div>
                    <p class="hidden-sm hidden-md hidden-lg"><a class="btn btn-dark" href="<?= get_sub_field("link_url") ?>"><?= get_sub_field("link_text") ?></a> <br/><br/></p>
                </div>
            </div>
            <div class="news-listing col-xs-12 col-sm-6 pull-right" style="z-index:1">
                <div class="news-text-content">
                    <h2>Aktuelle Mitteilungen</h2>
                </div>
                <div class="news-content">
                    <?php

                    $counter = 0;
                    $teaser_image = get_stylesheet_directory_uri() . '/assets/img/news-banner@2x.jpg';
                    $teaser_text = '';

                    $news_category = get_field('options_news_category', 'option');

                    $post_args = array(
                        'post_type'      => 'post',
                        'posts_per_page' => 2,
                        'post_status'    => 'publish',
                        'tax_query'   => array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'ID',
                                'terms'    => $news_category,
                            ),
                        ),
                    );

                    $posts = get_posts($post_args);

                    if (count($posts)) :
                        foreach ($posts as $post):

                            $x = setup_postdata($post);

                            ?>
                            <div class="content">
                                <hr>
                                <h2><small><?php echo get_the_date('d.m.Y')?></small> <?php the_title() ?></h2>
                                <p><?php echo wp_trim_words( $post->post_content) ?></p>
                                <a class="btn btn-dark" href="/aktuelles/#<?php echo $post->post_name; ?>">Beitrag weiterlesen</a>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata();
                    endif;
                    ?>
                    <p class="news-end hidden-sm hidden-md hidden-lg"><a class="btn btn-dark" href="<?php echo home_url('aktuelles/'); ?>">Alle
                            Mitteilungen auf einen Blick</a></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <div class="intro-box">
                    <p><a class="btn btn-dark" href="<?= get_sub_field("link_url") ?>"><?= get_sub_field("link_text") ?></a></p>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="news-content">
                    <div class="content">
                        <p class="news-end"><a class="btn btn-dark" href="<?php echo home_url('aktuelles/'); ?>">Alle
                                Mitteilungen auf einen Blick</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
