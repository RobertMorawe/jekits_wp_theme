<?php
?>

<section class="text-link-list<?=(get_sub_field('bg_color') == 1 ? ' bg-gray' : '')?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?=get_sub_field('text', true)?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?=get_sub_field('link_list', true)?>
            </div>
        </div>
    </div>
</section>