<section class="info-callout newsletter" id="newsletter">
	<div class="container">
		<div class="row">
<<<<<<< HEAD:templates/newsletter.php
			<div class="col-xs-12 col-md-12">
=======
			<div class="col-xs-12">
>>>>>>> origin/relaunch2018:templates/flex-content/newsletter.php
				<div class="intro-box">
					<?php if(get_sub_field('newsletter_formular') === "fortbildungsnewsletter"): ?>
					<h2><?php the_field('fortbildungsnewsletter_titel', 'option'); ?></h2>
					<p class="description dark"><?php the_field('fortbildungsnewsletter_untertitel', 'option'); ?></p>
					<p><?php the_field('fortbildungsnewsletter_text', 'option'); ?></p>
					<?php else: ?>
					<h2><?php the_field('stiftungsnewsletter_titel', 'option'); ?></h2>
					<p class="description dark"><?php the_field('stiftungsnewsletter_untertitel', 'option'); ?></p>
					<p><?php the_field('stiftungsnewsletter_text', 'option'); ?></p>
					<?php endif; ?>
                    <h4>Unser Datenschutz-Versprechen</h4>
                    <p>Durch Anklicken der unten stehenden Checkbox und das Anklicken der Schaltfläche
                        „Newsletter abonnieren“ erklären Sie sich damit einverstanden, dass wir Ihre E-Mail-Adresse
                        verarbeiten. Diese erfolgt ausschließlich für den Zweck der Zusendung des
                        Fortbildungsnewsletters. Eine Weitergabe an Dritte findet nur statt, soweit dies aus
                        technischen bzw. organisatorischen Gründen zwingend notwendig ist oder wir aus
                        rechtlichen Gründen hierzu verpflichtet sind. Ohne dieses Einverständnis ist eine Nutzung
                        des Newsletters leider nicht möglich. Wenn Sie den Newsletter abbestellen, enthält dieser
                        eine Abmeldefunktion, jeweils ganz am Ende der E-Mail. Wenn Sie sich dort wieder
                        abmelden, werden Ihre Daten nicht weiter verarbeitet.
                        Einzelheiten zum Thema Datenschutz finden Sie auch in unserer <a target="_blank" href="/datenschutz">Datenschutzerklärung.</a></p>

                </div>
			</div>
<<<<<<< HEAD:templates/newsletter.php
			<div class="col-xs-12 col-md-12">
=======
            <div class="col-xs-12">
                <div class="intro-box">

                <h4>Unser Datenschutz-Versprechen</h4>
                <p class="small">Durch Anklicken der unten stehenden Checkbox und das Anklicken der Schaltfläche „Newsletter abonnieren“ erklären Sie sich damit einverstanden, dass wir Ihre E-Mail-Adresse verarbeiten. Diese erfolgt ausschließlich für den Zweck der Zusendung des Fortbildungsnewsletters. Eine Weitergabe an Dritte findet nur statt, soweit dies aus technischen bzw. organisatorischen Gründen zwingend notwendig ist oder wir aus rechtlichen Gründen hierzu verpflichtet sind. Ohne dieses Einverständnis ist eine Nutzung des Newsletters leider nicht möglich. Wenn Sie den Newsletter abbestellen, enthält dieser eine Abmeldefunktion, jeweils ganz am Ende der E-Mail. Wenn Sie sich dort wieder abmelden, werden Ihre Daten nicht weiter verarbeitet. Einzelheiten zum Thema Datenschutz finden Sie auch in unserer <a href="/datenschutz">Datenschutzerklärung.</a></p>
                </div>

            </div>
            <div class="col-xs-12">
>>>>>>> origin/relaunch2018:templates/flex-content/newsletter.php
				<div class="newsletter-form">
					<?php /*<form action="">
						<input type="text" name="v_name" id="v_name" placeholder="Vorname">
						<input type="text" name="n_name" id="n_name" placeholder="Nachname">
						<input type="text" name="email" id="email" placeholder="E-Mail" required>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="sonder" value="sonder">
								<span class="check"></span> auch Sonderankündigungen
							</label>
						</div>
						<input type="submit" value="Newsletter abonnieren" >
					</form> */ ?>
					<?php if(get_sub_field('newsletter_formular') === "fortbildungsnewsletter"): ?>
					<?php echo do_shortcode(get_field('fortbildungsnewsletter_shortcode', 'option')); ?>
					<?php else: ?>
					<?php echo do_shortcode(get_field('stiftungsnewsletter_shortcode', 'option')); ?>
					<?php endif; ?>
<<<<<<< HEAD:templates/newsletter.php

=======
>>>>>>> origin/relaunch2018:templates/flex-content/newsletter.php
				</div>
			</div>
		</div>
	</div>
</section>