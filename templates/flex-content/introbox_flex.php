<?php
$color_hex = (get_sub_field('farbe') ? 'background-color: #' . get_sub_field('farbe') . ';' : '');
$extra_class = (get_sub_field('color_scheme') == 'darkblue' ? 'darkblue' : '')

?>
<section class="flex-content-box main-content <?= $extra_class ?>" style="<?php echo $color_hex; ?>">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <?php if (get_sub_field('title')): ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3><?= get_sub_field('title') ?></h3>
                        </div>
                    </div>
                <?php endif;

                if (get_sub_field('layout') == "col3"): ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col">
                            <?php flex_content_elements('column_1'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 col">
                            <?php flex_content_elements('column_2'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 col">
                            <?php flex_content_elements('column_3'); ?>
                        </div>
                    </div>
                <?php elseif (get_sub_field('layout') == "col2"): ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col">
                            <?php flex_content_elements('column_1'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col">
                            <?php flex_content_elements('column_2'); ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-xs-12 col">
                            <?php flex_content_elements('column_1'); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>