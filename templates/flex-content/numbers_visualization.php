<section class="flex-content-box main-content numbers-visualization">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <?php if (get_sub_field('title')): ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3><?= get_sub_field('title') ?></h3>
                    </div>
                </div>
                <?php endif;

                $numbers = get_sub_field('numbers');

                if ($numbers):
                    $number_col_width = 12/(count($numbers));
                ?>
                <div class="row">
                    <div class="col-xs-12 col">
                        <ul class="zahlen row">
                            <?php foreach ($numbers as $number):?>
                            <li class="col-xs-12 col-sm-<?=$number_col_width?>">
                                <figure class="chart"><figcaption><?=$number['row_0']?><span><?=$number['row_1']?></span></figcaption><svg width="200" height="200"><circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"></circle></svg></figure>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>