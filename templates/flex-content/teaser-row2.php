<?php

$show_contents = get_sub_field('show_contents');

if (is_array($show_contents) && !empty($show_contents)):

    $candidates = array(
        'instrumente' => array(
            'title'   => 'Instrumente',
            'img_src' => '/app/themes/jekits/assets/img/brand-icon-instrument.png',
        ),
        'tanzen'      => array(
            'title'   => 'Tanzen',
            'img_src' => '/app/themes/jekits/assets/img/brand-icon-dance.png',
        ),
        'singen'      => array(
            'title'   => 'Singen',
            'img_src' => '/app/themes/jekits/assets/img/brand-icon-sing.png',
        ),
    );

    ?>
    <section class="focus-row">
        <div class="flex-row">
            <?php
            $col_counter = 0;

            foreach ($candidates as $candidate => $config):
                if (in_array($candidate, $show_contents)):

                    if (!$col_counter && count($show_contents) < 3) {
                        $col_class = 'col-sm-offset-';
                        $col_class .= (12 - (count($show_contents) * 4)) / 2;
                    } else {
                        $col_class = '';
                    }

                    ?>
                    <div class="item col-xs-12 col-sm-4 <?= $col_class ?> <?=('col-'.$candidate)?>">
                        <a href="<?= get_field('its_link_' . $candidate, 'option') ?>">
                            <img class="icon-img" src="<?=$config['img_src']?>" alt="">
                        </a>
                        <div class="text">
                            <h2> <a href="<?= get_field('its_link_' . $candidate, 'option') ?>"><?= $config['title'] ?></a></h2>
                            <p><?= get_field('its_text_' . $candidate, 'option') ?></p>
                            <p><a class="btn btn-dark" href="<?= get_field('its_link_' . $candidate, 'option') ?>">JeKits – <?= $config['title'] ?></a></p>
                        </div>
                    </div>
                    <?php
                    $col_counter++;
                endif;
            endforeach;?>

        </div>
    </section>
<?php
endif; ?>