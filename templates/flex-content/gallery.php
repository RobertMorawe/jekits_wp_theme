<section class="gallery">
    <div class="image-gallery">
        <?php if (have_rows('galerie', 'option')): ?>
            <?php while (have_rows('galerie', 'option')): the_row();
                $image = get_sub_field('galerie_bild', 'option');
                if ($image['sizes']['large']):
                    ?>
                    <div class="image-slider">
                        <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt'] ?>"/>
                        <?php if ($image['description']): ?>
                            <p class="description"><?php echo $image['description']; ?></p>
                        <?php endif; ?>
                    </div>
                <?php
                endif;
            endwhile; ?>
        <?php endif; ?>
    </div>
</section>