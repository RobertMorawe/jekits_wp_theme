<section class="current-news">
    <div class="container-fluid">
        <div class="row flex-row">
            <div class="news-listing col-xs-12 col-sm-6 pull-right" style="z-index:1">
                <div class="news-text-content">
                    <h2>Aktuelle Mitteilungen</h2>
                </div>
                <div class="news-content">
                    <?php
                    $blog_terms = get_field('blog_posts_category');

                    $args = array(
                            'showposts' => 1,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'ID',
                                'terms'    => 66,
                            ),
                        ),
                    );

                    if (have_posts()) :

                        $counter = 0;
                        $teaser_image = get_stylesheet_directory_uri() . '/assets/img/news-banner@2x.jpg';
                        $teaser_text = '';

                        ?>
                        <?php query_posts('showposts=3'); // show one latest post only
                        while (have_posts()) : the_post();

                        if ($counter <= 0) {
                            if (get_the_post_thumbnail()) {
                                $teaser_image = get_the_post_thumbnail_url(get_the_ID(), 'large');
                                $teaser_text = get_the_title();
                            }
                        }
                        $counter++;

                        ?>
                        <div class="content">
                            <hr>
                            <h2><?php the_title(); ?></h2>
                            <?php the_excerpt(); ?>
                            <a class="btn read-more" href="/aktuelles/#/<?php echo $post->post_name; ?>">Beitrag
                                weiterlesen</a>
                        </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <p class="news-end"><a class="btn read-all" href="<?php echo home_url('aktuelles/'); ?>">Alle
                            Mitteilungen auf einen Blick</a></p>
                </div>
            </div>
            <div class="news-text col-xs-12 col-sm-6 pull-left">
                <div class="image gradient" style="background-image:url(<?php echo $teaser_image; ?>)">
                    <!--                    <img src="--><?php //echo $teaser_image; ?><!--" alt="">-->
                    <?php if ($teaser_text): ?>
                        <h3>
                            <?php echo $teaser_text; ?>
                        </h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php wp_reset_query(); ?>