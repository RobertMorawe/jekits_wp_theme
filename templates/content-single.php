<?php while (have_posts()) : the_post(); ?>
	<div class="page-header">
		<h1 class="entry-title center"><?php the_title(); ?></h1>
		<h3 class="center"><?php echo get_the_excerpt(); ?></h3>
	</div>
	<?php the_content(); ?>
<?php endwhile; ?>
