<section class="info-callout white btn-thin">
	 <div class="container">
	    <div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="intro-box">
						<h2>Ein neues Bildungsprogramm für NRW</h2>
						<p class="description dark">„JeKits – Jedem Kind Instrumente, Tanzen, Singen“ ist ein kulturelles Bildungsprogramm in der Grundschule für das Land Nordrhein-Westfalen. Durchgeführt wird JeKits in Kooperation von außerschulischen Bildungsinstitutionen (wie z. B. Musikschulen oder Tanzinstitutionen) und den Schulen. Mit dem Programm werden alle Kinder einer JeKits-Schule erreicht. <br><br>
                            JeKits hat drei alternative Schwerpunkte: Instrumente, Tanzen oder Singen. JeKits ist zum Schuljahr 2015/16 als landesweites Nachfolgeprogramm von „Jedem Kind ein Instrument“ (kurz JeKi) gestartet.
                            <a class="btn btn-dark" href="/programm/jekits/informationen-jekits/">JeKits – Jedem Kind Instrumente, Tanzen, Singen</a> <br /><br />
                        </p>


					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="brand-info">
						<a href="/programm/jekits/jekits-instrumente/">
							<img class="icon-img" src="app/themes/jekits/assets/img/brand-icon-instrument.png" height="233" width="166" alt="">
						</a>
						<a href="/programm/jekits/jekits-tanzen/">
							<img class="icon-img" src="app/themes/jekits/assets/img/brand-icon-dance.png" height="233" width="115" alt="">
						</a>
						<a href="/programm/jekits/jekits-singen/">
							<img src="app/themes/jekits/assets/img/brand-icon-sing.png" height="233" width="100" alt="">
						</a>
					</div>
				</div>
			</div>
    </div> <!-- Ende container -->
</section>
