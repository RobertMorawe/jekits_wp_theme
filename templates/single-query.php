<?php
	$pages = array(
	'post_type' => array ('page','fortbildung'),
	'order' => 'asc',
	'post_parent' => $post->ID
	);

	$queryObject = new WP_Query($pages);
?>

<?php
if ( $queryObject->have_posts() )
	while ( $queryObject->have_posts() ) : $queryObject->the_post();
?>
	<a id="<?php echo $post->post_name; ?>"></a>
	<?php if(get_field('is_introbox')): ?>
		<?php get_template_part('templates/introbox'); ?>
	<?php endif; ?>
	<?php $show_title = get_field('hide_title'); ?>
	<?php if( !empty($post->post_content) || ($show_title === null || $show_title != true) ): ?>
	<section class="main-content">
		<div class="container">
			<div class="content-wrap">
				<div class="content-box">
				<?php if( $show_title === null || $show_title != true ): ?>
					<h2><?php the_title(); ?></h2>
				<?php endif; ?>
				<?php the_content(); ?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php include('flex-content.php'); ?>
<?php endwhile; ?>
