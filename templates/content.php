<?php
$data_attr = '';
if ( !empty($post->post_name) ) {
	$data_attr = 'id="' . $post->post_name . '" ';
}

?>
<article <?php echo $data_attr; ?><?php post_class('accordion'); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry">
      <p class="date"><strong><?php echo get_the_date('d.m.Y')?></strong></p>
    <?php the_content(); ?>
  </div>
</article>
