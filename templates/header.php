<header class="banner" role="banner">
    <div class="navbar navbar-default navbar-static-top">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-header">
            <div class="container logo-container">
                <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
                    <div class="brand-logo"></div>
                </a>
                <?php if (is_front_page() && get_field('status', 'options')): ?>
                    <div class="notification">
                        <div class="notification-inner">
                            <h3><?php the_field('titel', 'options'); ?></h3>
                            <?php the_field('beschreibung', 'options');
                            if (get_field('show_link', 'options')):
                                ?>
                                <div class="btn-wrapper">
                                    <a class="btn btn-light" href="<?php the_field('verlinkung', 'options'); ?>"><?= (get_field('link_caption', 'options') ?: 'mehr Informationen') ?></a>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <nav class="nav-main" role="navigation">
            <div class="navbar-collapse collapse" id="navbar">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
                endif;
                ?>
            </div>
            <form class="searchbox" method="get" action="<?php echo home_url('/'); ?>">
                <input type="search" placeholder="Suchbegriff ..." name="s" class="searchbox-input" onkeyup="buttonUp();" value="<?php if (is_search()) {
                    echo get_search_query();
                }; ?>">
                <input type="submit" class="searchbox-submit <?php _e('Search', 'roots'); ?>" value="">
            </form>
        </nav>
    </div>
</header>