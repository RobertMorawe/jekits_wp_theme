<?php

$background_color_style = (get_field('farbe') ? 'background-color: #' . get_field('farbe') . ';' : '');

if(get_field('layout') == "col1"): ?>
		<section class="content-header" style="<?=$background_color_style?>">
			<div class="container">
				<div class="content-intro">
					<?php
						if( have_rows('intro_inhalt') ):
							while ( have_rows('intro_inhalt') ) : the_row();
								if( get_row_layout() == 'intro_text' ):
									echo '<h1>'.get_sub_field('headline').'</h1>';
									$light_content =  get_sub_field('content');
									if ( !empty($light_content) ) {
										echo '<p class="description light">' . $light_content . '</p>';
									}
									echo '<p class="outro">'.get_sub_field('content_small').'</p>';
								endif;
							endwhile;
						endif;
					?>
				</div>
			</div>
		</section>
	<?php endif;
	if(get_field('layout') == "col2"): ?>
		<section class="content-header-split">
					<?php

						if (get_field('ausrichtung') == "image"):
							$image_float = "left";
						 	$text_float = "right";
						elseif (get_field('ausrichtung') == "text"):
							$image_float = "right";
						 	$text_float = "left";
						endif;

						if( have_rows('inhalt_zweispaltig') ):
							while ( have_rows('inhalt_zweispaltig') ) : the_row();
								if( get_row_layout() == 'intro_text' ):
									echo '<div class="content-intro-50" >';
									echo '<h1>'.get_sub_field('headline').'</h1>';
									echo '<p class="description light">'.get_sub_field('content').'</p>';
									echo '<p class="outro">'.get_sub_field('content_small').'</p>';
									echo '</div>';

								elseif( get_row_layout() == 'content_image'):
									$image_src = get_sub_field('bild');
									echo '<div class="intro-image" style="float:' . $image_float . '"> <img src="' . $image_src['sizes']['large'] . '" alt=""></div>';
								endif;
							endwhile;
						endif;
					?>

		</section>
	<?php endif; ?>