<section class="teaser-row">
        <div class="flex-row">

            <div class="item first col-xs-12 col-sm-4">
                <a href="/fuer-programmpartner/" class="text">
                    <h2>Teilnahme am Programm</h2>
                    <p>Informationen zur Programmteilnahme und zum Bewerbungsverfahren für Kommunen, Grundschulen und
                        deren außerschulische Kooperationspartner</p>
                </a>
            </div>
            <div class="item col-xs-12 col-sm-4">
                <a href="/eltern/" class="text">
                    <h2>Informationen für Eltern</h2>
                    <p>Informationen zum Unterricht, zu Teilnahmebeiträgen und -befreiungen</p>
                </a>
            </div>
            <div class="item last col-xs-12 col-sm-4">
                <a href="/fuer-lehrkraefte/" class="text">
                    <h2>Informationen für Lehrkräfte</h2>
                    <p>Informationen zum Fort- und Weiterbildungsangebot inklusive Online-Anmeldung und
                        Newsletter-Abonnement</p>
                </a>
            </div>
        </div>
</section>
