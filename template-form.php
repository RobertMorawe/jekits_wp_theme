<?php
/*
Template Name: Anmeldeformular
 */
?>
<?php get_template_part('templates/page', 'header'); ?>
<section class="main-content">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('templates/content', 'page'); ?>
					<?php get_template_part('templates/accordion'); ?>
				<?php endwhile; ?>
            </div>
        </div>
    </div>
</section>