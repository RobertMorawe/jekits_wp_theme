<?php
/**
 * Roots includes
 *
 * The $roots_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/roots/pull/1042
 */
$roots_includes = array(
    'lib/utils.php',           // Utility functions
    'lib/init.php',            // Initial theme setup and constants
    'lib/wrapper.php',         // Theme wrapper class
    'lib/sidebar.php',         // Sidebar class
    'lib/config.php',          // Configuration
    'lib/activation.php',      // Theme activation
    'lib/titles.php',          // Page titles
    'lib/nav.php',             // Custom nav modifications
    'lib/gallery.php',         // Custom [gallery] modifications
    'lib/comments.php',        // Custom comments modifications
    'lib/scripts.php',         // Scripts and stylesheets
    'lib/extras.php',          // Custom functions
    'lib/flei_wp_toolkit.php',
    'configs/acf.php',         // Advanced Custom Fields settings (old way, now using acf-json folder)
);

foreach ($roots_includes as $file) {

    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

function flei_mc_msg($messages, $form_id)
{
//    if ($_SERVER['REMOTE_ADDR'] == '78.51.133.197'):

        if (isset($messages['success'])) {
            $messages['success']['text'] = 'Danke, wir haben Ihre Anmeldung erhalten! Bitte kontrollieren Sie Ihren Posteingang.';
        }
        
//    endif;

    return $messages;

}

add_filter('mc4wp_form_messages', 'flei_mc_msg', 10, 2);