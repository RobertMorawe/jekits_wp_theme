<?php
/*
Template Name: Fortbildung
*/
?>
<section class="content-header materialpool" style="background-color:#f1f1f1; margin-top:-50px;page-template-template-fortbildung" id="material">
    <div class="container">
        <div class="content-intro dark">
            <h1>JeKits-Materialpool</h1>
            <p class="description dark">Der JeKits-Materialpool ist ein geschlossener Bereich, der Unterrichtsmaterialien für alle drei Schwerpunkte (Instrumente, Tanzen, Singen) sowie Austauschmöglichkeiten für JeKits-Lehrkräfte anbietet. Ziel ist die Unterstützung und Vernetzung der Lehrkräfte. Der JeKits-Materialpool richtet sich nur an Lehrkräfte der außerschulischen Bildungspartner, die JeKits in NRW durchführen. Diese können sich hier anmelden:
            </p>
            <a href="https://materialpool.jekits.de" class="btn btn-dark">Zum JeKits-Materialpool</a>
            <p class="outro">
            </p>
        </div>
    </div>
</section>
<section class="main-content" id="fortbildungen">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <div class="page-header">
                    <h1>Fortbildungsangebot</h1>
                    <p class="center">„JeKits – Jedem Kind Instrumente, Tanzen, Singen“ ist zum Schuljahr 2015/16 gestartet. Unsere Fortbildungen richten sich an Lehrkräfte aus allen drei Schwerpunkten: Instrumente, Tanzen und Singen. Wir hoffen, dass sowohl NeueinsteigerInnen als auch KollegInnen mit jahrelanger Erfahrung interessante Angebote für sich entdecken.<br><br>
                        Die Fortbildungen richten sich grundsätzlich sowohl an Lehrkräfte der am JeKits-Programm beteiligten außerschulischen Bildungspartner (Musikschulen, Tanzinstitutionen etc.) als auch an die Lehrkräfte der Grund- und Förderschulen in NRW. Mehr denn je bieten die JeKits-Fortbildungen einen interdisziplinären Blick auf die Ansätze und Facetten einer lebendigen, vielfältigen Musizierpraxis mit Kindern.<br><br>
                        Die Fortbildungen sind für uns als Stiftung ein wichtiger direkter „Draht“ zu Ihnen als Lehrkräften. Die Erfahrungen aus der Praxis sichern in einem intensiven Austausch mit neuen Anstößen eine dauerhaft hohe Qualität der musik- und tanzpädagogischen Umsetzung vor Ort.
                    </p></div>
                <article class="accordion">
                    <header>
                        <h2 class="entry-title"><a href="#">Allgemeine Informationen</a></h2>
                    </header>
                    <div class="entry">
                        <div class="article-row">
                            <div class="description">
                                <h2>Zielgruppen</h2>
                                <p>Die Fortbildungen richten sich grundsätzlich sowohl an Lehrkräfte der am JeKits-Programm beteiligten außerschulischen Bildungspartner (Musikschulen, Tanzinstitutionen etc.) als auch an die Lehrkräfte der Grund- und Förderschulen in NRW. Die Zielgruppen der einzelnen Fortbildungsangebote ergeben sich aus dem jeweiligen Schwerpunkt. Viele der Angebote sind jedoch auch inhaltlich übergreifend gestaltet und nicht nach Schwerpunkten getrennt. Zur besseren
                                    Orientierung sind alle Fort- und Weiterbildungen mit einem entsprechenden Symbol für die jeweilige Zielgruppe versehen:</p>
                                <p>
                                    <img src="/app/themes/jekits/dist/img/icons/VA_instrument.gif" alt="Instrumente"> Instrumente<br>
                                    <img src="/app/themes/jekits/dist/img/icons/VA_tanzen.gif" alt="Tanzen"> Tanzen<br>
                                    <img src="/app/themes/jekits/dist/img/icons/VA_singen.gif" alt="Singen"> Singen<br>
                                    <img src="/app/themes/jekits/dist/img/icons/VA_schule.gif" alt="Grund- und Förderschulen"> Grund- und Förderschulen<br>
                                </p>
                                <p>Grundsätzlich müssen JeKits-Lehrkräfte der außerschulischen Bildungspartner einen pädagogischen und/oder einen künstlerischen Abschluss (z. B. Elementare Musikpädagogik, Instrumental- bzw. Gesangspädagogik, Tanzpädagogik, Bachelor of Music, Master of Music) oder einen vergleichbaren Abschluss (z. B. für das Lehramt Musik) an einer Hochschule für Musik und Tanz oder an einer vergleichbaren Institution (z. B. Universität, Musikakademie, Konservatorium) erworben
                                    haben. Die Fortbildungen der JeKits-Stiftung ergänzen die für JeKits relevanten Inhalte. Sie sind nicht als Ersatz für eine grundlegende künstlerisch-pädagogische Ausbildung konzipiert und können diese nicht ersetzen.</p>
                                <hr/>
                                <h2>Anmeldung/Teilnahme</h2>
                                <p>Die Anmeldung zu den Fortbildungen erfolgt über die Website der JeKits-Stiftung und ist verbindlich. Spätestens nach Ablauf der Anmeldefrist erhalten Sie eine Anmeldebestätigung mit weiteren Informationen per E-Mail. Sollten Sie trotz verbindlicher Anmeldung verhindert sein, melden Sie dies bitte umgehend bei der JeKits-Stiftung, damit der Fortbildungsplatz weiter vergeben werden kann.
                                </p>
                                <p>
                                    Die Fortbildungsteilnahme ist für die Lehrkräfte der am JeKits-Programm beteiligten außerschulischen Bildungspartner, Grund- und Förderschulen in NRW kostenfrei. Die Mindestteilnehmerzahl für die Durchführung der Lehrgänge und Seminare beträgt 6 TeilnehmerInnen, die Durchführung der Tagesfortbildungen ist an keine Mindestteilnehmerzahl gebunden. Die maximale Anzahl von TeilnehmerInnen ist bei den JeKits-Fortbildungen begrenzt. Die Platzvergabe richtet sich
                                    grundsätzlich nach der Reihenfolge der Anmeldungen sowie nach einer gleichmäßigen Verteilung auf die teilnehmenden Institutionen.
                                </p>
                                <hr>
                                <h2>Verpflegung/Unterkunft</h2>
                                <ul type="a">
                                    <li>
                                        Bei Tagesfortbildungen ist f&uuml;r Verpflegung selbst zu sorgen.
                                    </li>
                                    <li>
                                        Bei mehrt&auml;gigen Fortbildungen (JeKits-Akademie, JeKits-Forum &bdquo;insight&ldquo;, Qualifizierender Lehrgang, Kompaktseminare) organisiert die JeKits-Stiftung Unterkunft und Verpflegung.
                                    </li>
                                    <li>
                                        Die JeKits-Stiftung &uuml;bernimmt f&uuml;r alle Lehrkr&auml;fte der am JeKits-Programm beteiligten Bildungspartner, Grund- und F&ouml;rderschulen in NRW die Kosten f&uuml;r &Uuml;bernachtung und Verpflegung.
                                    </li>
                                    <li>
                                        Die &Uuml;bernachtung mit Verpflegung wird bei der Online-Anmeldung verbindlich mitgebucht. Bitte beachten Sie:<br>
                                        <strong>Eine kostenlose Stornierung ist nur bis zum Datum des Anmeldeschlusses m&ouml;glich.</strong><br>
                                        Bei Abmeldung nach Anmeldeschluss oder Nichterscheinen ohne rechtzeitige Abmeldung beh&auml;lt sich die JeKits-Stiftung vor, sich die ihr entstandenen Kosten von der Lehrkraft erstatten zu lassen. Es gelten folgende Fristen:
                                        <ul>
                                            <li>Bis zum Datum des Anmeldeschlusses ist eine kostenlose Stornierung m&ouml;glich.</li>
                                            <li>Bei Stornierung nach Anmeldeschluss werden der JeKits-Stiftung 40 % der &Uuml;bernachtungs- und Verpflegungskosten in Rechnung gestellt.</li>
                                            <li>Bei Stornierung nach 12 Uhr am Werktag vor der jeweiligen Veranstaltung oder bei Nichterscheinen ohne Mitteilung werden der JeKits-Stiftung 100 % der &Uuml;bernachtungs- und Verpflegungskosten in Rechnung gestellt.</li>
                                            <li>Die vorstehenden Regelungen gelten auch f&uuml;r mehrphasige Fortbildungen. Die Fristen beziehen sich f&uuml;r diese F&auml;lle aber nicht auf den Anmeldeschluss, sondern auf den Veranstaltungstag. Eine kostenlose Stornierung ist Monat vor dem Veranstaltungstag m&ouml;glich.</p>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p style="margin-bottom:initial">Die Kosten f&uuml;r &Uuml;bernachtung/Verpflegung betragen im Fortbildungsjahr 2019:<br />Qualifizierender Lehrgang/Intensivseminare:</p>
                                        <ul>
                                            <li>Landesmusikakademie NRW: 75,50 &euro; pro Modul</li>
                                            <li>Akademie der Kulturellen Bildung: 74,40 &euro; pro Modul</li>
                                        </ul>
                                        <p style="margin-bottom:initial">Kompaktseminare:</p>
                                        <ul>
                                            <li>Musikbildungszentrum S&uuml;dwestfalen: 86,50 &euro;</li>
                                            <li>Landesmusikakademie NRW: 67,20 &euro;</li>
                                        </ul>



                                        <p style="margin-bottom:initial"><br>Die Kosten f&uuml;r &Uuml;bernachtung/Verpflegung betragen im Fortbildungsjahr 2020:</p>
                                        <p style="margin-bottom:initial">Qualifizierender Lehrgang:</p>
                                        <ul>
                                            <li>Landesmusikakademie NRW: 87,20 &euro; pro Modul</li>
                                        </ul>
                                        <p style="margin-bottom:initial">JeKits-Forum <em>&bdquo;insight&ldquo;</em>:</p>
                                        <ul>
                                            <li>Musikbildungszentrum S&uuml;dwestfalen: 81,00 &euro; pro Modul</li>
                                        </ul>
                                        <p style="margin-bottom:initial">Kompaktseminare:</p>
                                        <ul>
                                            <li>Landesmusikakademie NRW: 76,80 &euro;</li>
                                            <li>Musikbildungszentrum S&uuml;dwestfalen: 86,50 &euro;</li>
                                            <li>Kolping Bildungswerk Essen: 81,30 &euro;</li>
                                        </ul>
                                    </li>
                                </ul>
                                <hr>
                                <h2>Externe Lehrkräfte</h2>
                                <p>
                                    Als externe Lehrkräfte gelten Lehrkräfte aus NRW, die nicht bei einem am JeKits-Programm beteiligten außerschulischen Bildungspartner oder einer beteiligten Grund- oder Förderschule beschäftigt sind. Externe Lehrkräfte können an den Fortbildungen teilnehmen, wenn nach Anmeldeschluss noch Plätze frei sind. Für interessierte externe Lehrkräfte steht ein gesondertes Anmeldeformular innerhalb der Online-Anmeldung zur Verfügung. Bei mehrtägigen Fortbildungen sind
                                    die oben genannten anfallenden Übernachtungs- und Verpflegungskosten von externen TeilnehmerInnen selbst zu tragen.
                                </p>
                                <hr/>
                                <h2>Haftung</h2>
                                <p>Wir bitten um Verständnis dafür, dass die JeKits-Stiftung im Rahmen der Fortbildungen keine Haftung für leicht fahrlässig verursachte Sachschäden übernimmt.</p>
                                <hr/>
                                <h2>Ansprechpartner</h2>
                                <p>JeKits-Stiftung <br>
                                    Martin Theile (Fachberater)<br>
                                    Kortumstraße 17<br>
                                    44787 Bochum<br>
                                    Tel: 0234.541747-32<br>
                                    Fax: 0234.541747-99<br>
                                    <a href="mailto:fortbildung@jekits.de">fortbildung@jekits.de</a></p>
                                <hr/>
                            </div>
                        </div>
                    </div>
                </article>
                <?php
                /**
                 * Fetch all categories and loop related posts
                 */
                $args = array(
                    'type'     => 'fortbildung',
                    'order'    => 'ASC',
                    'taxonomy' => 'fortbildung_category',
                );
                $categories = get_categories($args);
                ?>
                <?php foreach ($categories as $key => $category): ?>
                    <article <?php post_class('accordion fortbildung'); ?> id="<?= sanitize_title($category->name) ?>">
                        <header>
                            <h2 class="entry-title"><a href="#"><?php echo $category->name; ?></a></h2>
                        </header>
                        <div class="entry">
                            <?php
                            $posts = query_posts(array(
                                'post_type'      => 'fortbildung',
                                'posts_per_page' => -1,
                                'meta_key'       => 'termine_0_date_from',
                                'orderby'        => 'meta_value_num',
                                'order'          => 'ASC',
                                'tax_query'      => array(
                                    array(
                                        'taxonomy' => 'fortbildung_category',
                                        'terms'    => $category->term_id,
                                        'field'    => 'term_id',
                                    ),
                                ),
                            ));
                            ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="article-row">
                                    <div class="description">
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_excerpt(); ?></p>
                                    </div>
                                    <div class="dates">
                                        <?php
                                        $dates = get_field('termine');
                                        ?>
                                        <h3><?php echo __('Termin'.(count($dates) > 1?'e':'').':', 'roots'); ?></h3>
                                        <ul>
                                            <?php foreach ($dates as $key => $date): ?>
                                                <li><?php echo ($date['title']?$date['title'].': ':''); ?><?php echo get_merged_date($date['date_from'], $date['date_to']); ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <div class="more">
                                        <?php

                                        $btn_text = __('Info & Anmeldung');
                                        if (get_field('form_page_id')) {
                                        } else {
//                                            $btn_text = __('Info');
                                        }
                                        ?>
                                        <a class="button" href="<?php echo get_permalink(); ?>" ?><?php echo $btn_text; ?></a>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </article>
                <?php endforeach; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<?php //include('templates/single-query.php'); ?>
<?php wp_reset_query(); ?>
