<?php
/*
Template Name: Fallback
*/
?>

<style type="text/css">
	.wrap {
		font-family: Arial;
	}

	.wrap h1, .wrap h2, .wrap h3, .wrap .content-box h3 {
		font-family: Arial;
	}
</style>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('templates/page', 'header'); ?>
					<?php get_template_part('templates/content', 'page'); ?>
					<?php get_template_part('templates/accordion'); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
