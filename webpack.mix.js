let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// mix.setPublicPath('dist/');
mix.options({
    // extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
    processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
    // purifyCss: false, // Remove unused CSS selectors.
    // uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
    // postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
});
mix.sourceMaps(); // Enable sourcemaps

mix.combine(
    [
        'assets/vendor/bootstrap/js/tooltip.js',
        'assets/vendor/bootstrap/js/transition.js',
        'assets/vendor/bootstrap/js/collapse.js',
        'assets/vendor/bootstrap/js/carousel.js',
        'assets/vendor/bootstrap/js/modal.js',
        'assets/vendor/picturefill/dist/picturefill.js',
        'assets/vendor/retina.js/dist/retina.js',
        'assets/vendor/jquery.stellar/src/jquery.stellar.js',
        'assets/vendor/slick.js/slick/slick.js',
        'assets/vendor/gmaps/gmaps.js',
        'assets/js/plugins/*.js',
        'assets/js/vendor/gmaps-markerclusterer.js',
        'assets/js/_*.js'
    ],
    'dist/main.js')
    .minify('dist/main.js')
    .copy('assets/img/', 'dist/img/')
    .copy('assets/fonts/', 'dist/fonts/')
    .less('assets/less/main.less', 'dist/main.css')
    .minify('dist/main.css');

    mix.browserSync({
        proxy: 'jekits.docker',
        files: ['**/*.php','dist/main.min.css', 'dist/main.min.js']
    });

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('jekits.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('dist/');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
  // extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
  // processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
  // purifyCss: false, // Remove unused CSS selectors.
  // uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
  // postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
