<?php
/*
Template Name: Homepage
*/
?>
<section class="master">
    <?php require_once('partials/language.php'); ?>
    <div class="container">
        <div class="intro-text">
            <div class="centered">
                <p>
                <h1>JeKits</h1>
                <h2>Jedem Kind Instrumente, Tanzen, Singen</h2>
                </p>
            </div>
        </div>
    </div>
    <header>
        <?php

        if (get_field('header_slider_enabled') && $header_slider_images = get_field('header_slider_images')):?>
            <div id="home-header-carousel" class="carousel slide" data-ride="carousel">
                <?php

                $indicators = '';
                $slides = '';

                foreach ($header_slider_images as $idx => $slide):

                    $indicators .= '<li data-target="#home-header-carousel" data-slide-to="' . $idx . '"' . ($idx == 0 ? ' class="active"' : '') . '></li>';
                    $slides .= '<div class="item' . ($idx == 0 ? ' active' : '') . '">' . wp_get_attachment_image($slide['image'], 'home_header_carousel') . '</div>';

                endforeach; ?>
                <ol class="carousel-indicators">
                    <?= $indicators ?>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?= $slides ?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#home-header-carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#home-header-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        <?php else: ?>
            <div class="static-header">
                <div class="static-slide slide slide-two" data-stellar-background-ratio="0.6" data-stellar-horizontal-offset="0" data-stellar-vertical-offset="0"></div>
                <div class="static-slide slide slide-one show" data-stellar-background-ratio="0.6" data-stellar-horizontal-offset="0" data-stellar-vertical-offset="0"></div>
            </div>
        <?php endif; ?>
    </header>
</section>
