<div class="result-filter">
    <h4>Filtermöglichkeiten</h4>
    <div class="filter-items">
        <span class="filter_item label label-instrumente" data-filter="instrumente">Instrumente</span>
        <span class="filter_item label label-tanzen" data-filter="tanzen">Tanzen</span>
        <span class="filter_item label label-singen" data-filter="singen">Singen</span>
        <span class="filter_item label label-koop" data-filter="koop">Bildungspartner</span>
    </div>
</div>

<?php if ( $query->have_posts() ) : ?>
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <?php
            $field = get_field_object('typ');
            $value = get_field('typ');
            $label = $field['choices'][ $value ];
         ?>

        <article class="school" data-type="<?php echo $value ?>">
            <h3 class="entry-title"><?php the_title(); ?></h3>
            <p><?php the_field('straße'); ?> <?php the_field('hausnummer'); ?><br /><?php the_field('zip'); ?> <?php the_field('stadt_kommune'); ?><br />
            <a href="<?php the_field('webseite'); ?>"><?php the_field('webseite'); ?></a></p>

            <p><span class="label label-<?php echo $value ?>"><?php echo $label ?></span></p>
        </article>
        <?php
            $coordinate = array();
            $acf_map = get_field('map');

            if ( !empty($acf_map) ) {
                $coordinate = get_field('map');
                $coordinate['type'] = 'latlng';
            }
            else {
                $coordinate['type'] = 'address';
            }

            $coordinate['marker'] = 'DF3505';

            if ( get_field('typ') == 'instrumente' ) {
                $coordinate['marker'] = 'E1270E';
                $coordinate['school_type'] = 'instrumente';
            } else if ( get_field('typ') == 'singen' ) {
                $coordinate['marker'] = '9BBC0B';
                $coordinate['school_type'] = 'singen';
            } else if ( get_field('typ') == 'tanzen' ) {
                $coordinate['marker'] = '009EE3';
                $coordinate['school_type'] = 'tanzen';
            } else if ( get_field('typ') == 'koop' ) {
                $coordinate['marker'] = '0A2A68';
                $coordinate['school_type'] = 'koop';
            }


            $coordinate['name'] = get_the_title();
            $coordinate['address'] = get_field('straße') . ' ' . get_field('hausnummer') . ', ' . get_field('zip') . ' ' . get_field('stadt_kommune');

            array_push($coordinates, $coordinate);
        ?>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Leider haben wir keine Schulen nach diesem Suchkriterium finden können.' ); ?></p>
<?php endif; ?>
