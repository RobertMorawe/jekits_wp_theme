<?php if ( $query->have_posts() ) : ?>
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <article class="school">
            <h3 class="entry-title"><?php the_title(); ?></h3>
            <p><?php the_field('street'); ?> <?php the_field('street_number'); ?><br /><?php the_field('zip'); ?> <?php the_field('city'); ?><br />
            <a href="http://<?php the_field('webseite'); ?>"><?php the_field('webseite'); ?></a></p>
        </article>
        <?php
            $coordinate = array();
            $acf_map = get_field('map');

            if ( !empty($acf_map) ) {
                $coordinate = get_field('map');
                $coordinate['type'] = 'latlng';
            }
            else {
                $coordinate['type'] = 'address';
            }

            $coordinate['marker'] = 'DF3505';

            if ( get_field('typ') === 'musicschool' ) {
                $coordinate['marker'] = '00A6EB';
            }

            $coordinate['name'] = get_the_title();
            $coordinate['address'] = get_field('street') . ' ' . get_field('street_number') . ', ' . get_field('zip') . ' ' . get_field('city');

            array_push($coordinates, $coordinate);
        ?>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Leider haben wir keine Schulen nach diesem Suchkriterium finden können.' ); ?></p>
<?php endif; ?>
