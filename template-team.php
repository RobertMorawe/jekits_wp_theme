<?php
/*
Template Name: Team
*/
?>
<?php get_template_part('templates/page', 'header'); ?>
<section class="main-content team">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">

				<?php
					if( have_rows('team_section') ):

					// loop through the rows of data
						while ( have_rows('team_section') ) : the_row();

							if( get_row_layout() == 'abteilung' ):
								echo '<div class="team__members">';
									echo '<h2>'  . get_sub_field('titel') . '</h2>';

									if( have_rows('team-mitglied') ):
										echo '<div class="row">';

										while ( have_rows('team-mitglied') ) : the_row();

											echo '<div class="col-md-6 team__members__col">';
												if (get_sub_field('portrait')):
													$image = get_sub_field('portrait');
													echo '<img src="' . $image . '" />';
												endif;
												echo '<div class="team__members__information">';
												echo '<h3>'  . get_sub_field('team-name') . '</h3>';
												the_sub_field('personliche_daten');
												echo '</div>';
											echo '</div>';

										endwhile;
										echo '</div>';
									endif;

								echo '</div>';
							endif;

							if ( get_row_layout() == 'abteilung_solo'):
								echo '<div class="team__members">';
									echo '<h2>'  . get_sub_field('titel_solo') . '</h2>';
									if( have_rows('team-mitglied_s') ):
										echo '<div class="row">';

										while ( have_rows('team-mitglied_s') ) : the_row();

											echo '<div class="col-md-12 team__members__col">';

												echo '<div class="col-md-6 no-padding">';
													if (get_sub_field('portrait_img_s')):
														$image = get_sub_field('portrait_img_s');
														echo '<img src="' . $image . '" />';
													endif;
												echo '</div>';

												echo '<div class="col-md-6 team__members--align">';
													echo '<div class="team__members__information">';
													echo '<h3>'  . get_sub_field('name_s') . '</h3>';
													the_sub_field('personliche_informationen');
													echo '</div>';
												echo '</div>';
											echo '</div>';

										endwhile;
										echo '</div>';
									endif;
								echo '</div>';
							endif;

						endwhile;
				endif;

			 ?>

			</div>
		</div>
	</div>
</section>