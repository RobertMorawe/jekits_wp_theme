 /**
  * Expandable Search-Input
  */
jQuery(document).ready(function($){

	$('.nav-main').each(function() {

		var submitIcon = $(this).find('.icon-search'),
				inputBox = $(this).find('.searchbox-input'),
				searchBox = $(this).find('.searchbox'),
				searchSubmit = $(this).find('.searchbox-submit'),
				isOpen = false;

		submitIcon.on('click', function(e) {
			if(isOpen === false) {
				searchBox.addClass('searchbox-open');
				inputBox.css('display', 'block');
				$(this).addClass('active-search');

				inputBox.trigger('input');
				inputBox.focus();
				isOpen = true;
			}
			else {
				searchBox.removeClass('searchbox-open');
				inputBox.focusout();

				inputBox.css('display', 'none');
				searchSubmit.css('display', 'none');

				$(this).removeClass('active-search');

				isOpen = false;
			}

			e.preventDefault();
		});

		inputBox.on('input', function() {
			var inputVal = inputBox.val();
					inputVal = $.trim(inputVal).length;

			if( inputVal !== 0) {
				searchSubmit.css('display','block');
			}
			else {
				inputBox.val('');
				searchSubmit.css('display', 'none');
			}
		});
	});

});