 /**
  * Simple Slide/Fade-Show
  */
jQuery(document).ready(function($){

	var $master = $('.master');

	if ( $master.length > 0 && $master.find('.slide').length > 1 ) {

		setInterval(function() {
			$master.find('.slide').each(function() {
				if (!$(this).hasClass('show')) {
					$master.find('.slide').removeClass('show');
					$(this).addClass('show');
					return false;
				}
			});
		}, 8000);
	}
});