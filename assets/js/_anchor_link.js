/**
 * Accordion Anchor Link
 */
jQuery(document).ready(function($){

	var hash_changed = function( hash ) {
		var parsed_hash = hash.replace('#', '').replace('/', ''),
				target_item = $('[data-name="' + parsed_hash + '"]');

		if ( target_item.length < 1 ) {
			return false;
		}

		$('html, body').delay( 500 ).animate({
			scrollTop: target_item.offset().top - 35
		},
		400,
		function() {
			if ( target_item.hasClass('open') ) {
				return false;
			}

			target_item.find('.entry-title > a').click();
		});
	};

	if ("onhashchange" in window) { // event supported?
		window.onhashchange = function () {
			hash_changed(window.location.hash);
		};
	}

	if (window.location.hash.length > 0) {
		hash_changed(window.location.hash);
	}
});