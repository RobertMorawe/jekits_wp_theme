/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

<<<<<<< HEAD
(function($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
var Roots = {
  // All pages
  common: {
    init: function() {
      // JavaScript to be fired on all pages
      var self = this;

      $.mobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

      $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});

			/**
			 * Slick init
			 */
			if ( $('.image-gallery').length > 0 ) {

				var slides_to_show = 2;

				if ($.mobile) {
					slides_to_show = 1;
				}

				$('.image-gallery').slick({
					slidesToShow: slides_to_show,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '20px',
					focusOnSelect: true
				});
			}


			/**
			 * Stellar init
			 */
			if ( !$.mobile ) {
				$.stellar({
					horizontalScrolling: false,
					verticalOffset: 0,
					horizontalOffset: 0,
					hideDistantElements: true
				});
			}


			/**
			 * Display notification
			 */
			var notification = $('.notification');
			if ( notification.length > 0 ) {
				notification.addClass('show');
			}

			/**
			 * Menu
			 */
			var $menu = $('.menu-das-programm');
			if ( $menu.length > 0 ) {
				var collapse_items = $menu.find('.programm .collapse > a');

				collapse_items.on('click', function(e) {
					// e.preventDefault();
					// collapse_items.parent().find('.collapse-content').addClass('collapsed');
					// $(this).parent().find('.collapse-content').removeClass('collapsed');

					// collapse_items.parent().closest('li.collapse').addClass('collapsed');
					// $(this).parent().closest('li.collapse').removeClass('collapsed');
				});

				$('.navbar-nav a').on('click', function(e) {
					if ( $(this).attr('href') === '#' ) {
						e.preventDefault();
					}
				});
			}


			/**
			 * neighbourhood search
			 */
			var $search_forms = $('.neighbourhood-search form, .neighbourhood form');
			if ( $search_forms.length > 0 ) {
				$search_forms.each(function() {
					var $form = $(this);

					var check_value = function() {
						var value = $(this).val();

						if ( value.length > 2 ) {
							$form.find('input[type="submit"]').removeClass('hide');
						}
					};

					$form.find('.zip, .name_place').each(check_value);
					$form.find('.zip, .name_place').on('change paste keyup', check_value);
				});
			}

			this.maps();
			this.filter();
			this.accordion();

    },
    /**
     * Filter the jekits school search results based on their type.
     * @return {[type]} [description]
     */
     filter: function() {
    	var filter_items = $('.filter_item');
    	var school_list = $('.school');

    	if ( filter_items.length <= 0 ) {
    		return false;
    	}

    	// Array list with active items
    	var active = [];

    	// Iterate through each filter button.
    	filter_items.each( function() {
    		$(this).on('click', function(e) {
    			$(this).toggleClass('active');
    			// Current item that should be filtered
    			var parent_data_filter = $(this).data("filter");

    			// Check if the current item is active.
    			// Then push it to the active array.
    			// Else remove it from the array.

    			if ($(this).hasClass('active')) {
    				active.push ($(this).data("filter"));

    			} else {
    				var index = active.indexOf($(this).data("filter"));
    				if (index > -1) {
    					active.splice(index, 1);
    				}
    			}

    			// Iterate through each school shown on the result page.
    			school_list.each( function() {

    				var currentDataItem =  $(this).data("type");
    				var isInArray = active.indexOf(currentDataItem);

    				if ( currentDataItem === parent_data_filter && isInArray !== -1) {
    					$(this).show('slow');
    					Roots.common.maps.toggleMarker(currentDataItem, true);

    				} else if (isInArray !== -1) {
    					$(this).show('slow');
    					Roots.common.maps.toggleMarker(currentDataItem, true);

    				} else {
    					$(this).hide('slow');
    					Roots.common.maps.toggleMarker(currentDataItem, false);
    				}

    				if (active.length === 0) {
    					Roots.common.maps.toggleMarker("", true);
    					$(this).toggle('slow');

    				}
    			});
    			e.preventDefault();
    		});
    	});
    },
    accordion: function() {
    	var accordion_items = $('.accordion');

    	if ( accordion_items.length <= 0 ) {
    		return false;
    	}

    	accordion_items.each(function() {
    		var $this = $(this);

    		$this.find('.entry-title a').on('click', function(e) {
    			$this.toggleClass('open');
    			e.preventDefault();
    		});
    	});
    },
    maps: function() {
    	var map_id = '#map',
    			$map = $(map_id),
    			marker = {},
    			map = null,
    			center_map = true; // Google Maps object

			if ( $map.length <= 0 || typeof schools === 'undefined' ) {
				// console.info('No latitude / longitude defined!');
				return false;
			}
			var instrumentMarker = [];
			var	singenMarker = [];
			var tanzenMarker = [];
            var koopMarker = [];

			marker = schools;
			var map_config = {
				div: map_id,
				zoom: 13,
				scrollwheel: false
			};

			if ( typeof marker[0].lat !== 'undefined' && typeof marker[0].lng !== 'undefined' ) {
				map_config.lat = marker[0].lat;
				map_config.lng = marker[0].lng;
				center_map = false;
			}
			else {
				map_config.lat = '51.4677489';
				map_config.lng = '7.36657439999999';
			}
			map = new GMaps(map_config);

			$.each(marker, function(key, value) {
				var mark = value,
						i = key;

				var pinColor = mark.marker;
				var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
					new google.maps.Size(21, 34),
					new google.maps.Point(0,0),
					new google.maps.Point(10, 34)
		    );

				// Create new Marker and push it to the repsonding array,
				// based on school_type.
         if ( mark.type === 'address' ) {
          GMaps.geocode({
            address: mark.address,
            callback: function(results, status) {
              if (status === 'OK') {
                var latlng = results[0].geometry.location;
                if ( center_map ) {
                  map.setCenter(latlng.lat(), latlng.lng());
                  center_map = false;
                }

                var marker = map.addMarker({
                  lat: latlng.lat(),
                  lng: latlng.lng(),
                  title: mark.name,
                  icon: pinImage,
                  infoWindow: {
                    content: '<b>' + mark.name + '</b><br />' + mark.address
                  }
                });

                if (mark.school_type === 'instrumente') {
                  instrumentMarker.push(marker);
                }

                if (mark.school_type === 'tanzen') {
                  tanzenMarker.push(marker);
                }

                if (mark.school_type === 'singen') {
                  singenMarker.push(marker);
                }

                if(mark.school_type === 'koop') {
                    koopMarker.push(marker);
                }
              }
            }
          });
        }
        else {
				var marker = map.addMarker({
					lat: mark.lat,
					lng: mark.lng,
					icon: pinImage,
					animation: google.maps.Animation.DROP,
					infoWindow: {
									content: '<b>' + mark.name + '</b><br />' + mark.address
								}
				});

          if (mark.school_type === 'instrumente') {
            instrumentMarker.push(marker);
          }

          if (mark.school_type === 'tanzen') {
            tanzenMarker.push(marker);
          }

          if (mark.school_type === 'singen') {
            singenMarker.push(marker);
          }

          if(mark.school_type === 'koop') {
              koopMarker.push(marker);
          }
        }
			});

			/**
			 * Toogle Marker on map based on their type (instrumente, tanzen, singen).
			 * And the second parameter indicates if it should get shown or hidden.
			 *
			 * @param  Array type instrumente, tanzen, singen
			 * @param  bool show true, false
			 * @return {[type]}      [description]
			 */
			Roots.common.maps.toggleMarker =  function (type, show) {
				var currentType;
				console.log("show marker");

				if (type === 'instrumente') {
					currentType = instrumentMarker;
				}

				if (type === 'singen') {
					currentType = singenMarker;
				}

				if (type === 'tanzen') {
					currentType = tanzenMarker;
				}

                if (type === 'koop') {
                    currentType = koopMarker;
                }

				if (type === "") {
					currentType = instrumentMarker.concat(tanzenMarker, singenMarker, koopMarker) ;
				}

				$.each(currentType, function(){
					if(show === true) {
						this.setVisible(true);
					} else {
						this.setVisible(false);
					}

				});
			};
	}

  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

jQuery(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
=======
(function ($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
    var Roots = {
            // All pages
            common: {
                init: function () {
                    // JavaScript to be fired on all pages
                    var self = this;

                    var isMobile = false; //initiate as false
// device detection
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                        isMobile = true;
                    }

                    // $.mobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
                    $.mobile = isMobile;

                    $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});

                    /**
                     * Slick init
                     */
                    if ($('.image-gallery').length > 0) {

                        var slides_to_show = 2;

                        if ($.mobile) {
                            slides_to_show = 1;
                        }

                        $('.image-gallery').slick({
                            slidesToShow: slides_to_show,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '20px',
                            focusOnSelect: true
                        });
                    }


                    // prevent opening top links if menu item has dropdown
                    if ($.mobile) {
                        $('a.dropdown-toggle').click(function (e) {
                            e.preventDefault();
                        });
                    }

                    /**
                     * Stellar init
                     */
                    if (!$.mobile) {
                        $.stellar({
                            horizontalScrolling: false,
                            verticalOffset: 0,
                            horizontalOffset: 0,
                            hideDistantElements: true
                        });
                    }


                    /**
                     * Display notification
                     */
                    var notification = $('.notification');
                    if (notification.length > 0) {
                        notification.addClass('show');
                    }

                    /**
                     * Menu
                     */
                    var $menu = $('.menu-das-programm');
                    if ($menu.length > 0) {
                        var collapse_items = $menu.find('.programm .collapse > a');

                        collapse_items.on('click', function (e) {
                            // e.preventDefault();
                            // collapse_items.parent().find('.collapse-content').addClass('collapsed');
                            // $(this).parent().find('.collapse-content').removeClass('collapsed');

                            // collapse_items.parent().closest('li.collapse').addClass('collapsed');
                            // $(this).parent().closest('li.collapse').removeClass('collapsed');
                        });

                        $('.navbar-nav a').on('click', function (e) {
                            if ($(this).attr('href') === '#') {
                                e.preventDefault();
                            }
                        });
                    }


                    /**
                     * neighbourhood search
                     */
                    var $search_forms = $('.neighbourhood-search form, .neighbourhood form');
                    if ($search_forms.length > 0) {
                        $search_forms.each(function () {
                            var $form = $(this);

                            var check_value = function () {
                                var value = $(this).val();

                                if (value.length > 2) {
                                    $form.find('input[type="submit"]').removeClass('hide');
                                }
                            };

                            $form.find('.zip, .name_place').each(check_value);
                            $form.find('.zip, .name_place').on('change paste keyup', check_value);
                        });
                    }

                    $search_forms.each(function () {
                        var terms_agreed = $(this).parents('.neighbourhood-search, .neighbourhood').find('#terms_agreed');
                        $(this).on('submit', function (e) {

                            if (!terms_agreed || !terms_agreed.prop('checked')) {
                                terms_agreed.parents('.agree_terms').addClass('has-error');
                                return false;
                            } else {
                                terms_agreed.parents('.agree_terms').removeClass('has-error');
                            }
                        });

                        terms_agreed.on('change', function () {
                            if ($(this).prop('checked')) {
                                $(this).parents('.agree_terms').removeClass('has-error');
                            }
                        });
                    });


                    /**
                     * Open Accordion items if they are linked to through the anchor
                     */


                    if (window.location.hash) {

                        var linked_accordion_item = $('.accordion' + window.location.hash);

                        if (linked_accordion_item.length) {
                            $(linked_accordion_item).each(function () {
                                $(this).addClass('open');
                            })
                        }

                    }


                    /**
                     * Play Button
                     */

                    // $('.play-btn a').on('click', function(e) {
                    //     // e.preventDefault();
                    //
                    //     let modal = $(this).data('modal');
                    //     if (modal) {
                    //         // $(modal).modal('toggle');
                    //     }
                    //
                    //
                    // });

                    $('.play-btn a').each(function () {
                        var modal = $(this).data('target');
                        if (modal) {
                            $(modal).on('hidden.bs.modal', function (e) {

                                // reset iframe
                                var iframe = $(this).find('iframe');
                                var old_src = $(iframe).attr('src');
                                $(iframe).attr('src', '');
                                $(iframe).attr('src', old_src);

                            })
                        }
                    });


                    /** Animate number chart circles once they get into viewport **/

                    $(window).on('scroll', function () {

                            $('.chart').not('shown').each(function (idx) {

                                // console.log($(this).offset().top, $(document).scrollTop(), $(document).scrollTop() + $(window).height(), $(this).offset().top > $(document).scrollTop() && $(this).offset().top < $(document).scrollTop() + $(window).height());


                                if ($(this).offset().top > $(document).scrollTop() && $(this).offset().top < $(document).scrollTop() + $(window).height()) {
                                    //
                                    var that = this;
                                    setTimeout(function () {
                                        $(that).addClass('shown');
                                    }, 250 * (idx + 1));
                                }
                                else {
                                    // $(this).removeClass('shown');
                                }
                            });
                        }
                    );


                    this.maps();
                    this.filter();
                    this.accordion();


                },
                /**
                 * Filter the jekits school search results based on their type.
                 * @return {[type]} [description]
                 */
                filter: function () {
                    var filter_items = $('.filter_item');
                    var school_list = $('.school');

                    if (filter_items.length <= 0) {
                        return false;
                    }

                    // Array list with active items
                    var active = [];

                    // Iterate through each filter button.
                    filter_items.each(function () {
                        $(this).on('click', function (e) {
                            $(this).toggleClass('active');
                            // Current item that should be filtered
                            var parent_data_filter = $(this).data("filter");

                            // Check if the current item is active.
                            // Then push it to the active array.
                            // Else remove it from the array.

                            if ($(this).hasClass('active')) {
                                active.push($(this).data("filter"));

                            } else {
                                var index = active.indexOf($(this).data("filter"));
                                if (index > -1) {
                                    active.splice(index, 1);
                                }
                            }

                            // Iterate through each school shown on the result page.
                            school_list.each(function () {

                                var currentDataItem = $(this).data("type");
                                var isInArray = active.indexOf(currentDataItem);

                                if (currentDataItem === parent_data_filter && isInArray !== -1) {
                                    $(this).show('slow');
                                    Roots.common.maps.toggleMarker(currentDataItem, true);

                                } else if (isInArray !== -1) {
                                    $(this).show('slow');
                                    Roots.common.maps.toggleMarker(currentDataItem, true);

                                } else {
                                    $(this).hide('slow');
                                    Roots.common.maps.toggleMarker(currentDataItem, false);
                                }

                                if (active.length === 0) {
                                    Roots.common.maps.toggleMarker("", true);
                                    $(this).toggle('slow');

                                }
                            });
                            e.preventDefault();
                        });
                    });
                }
                ,
                accordion: function () {
                    var accordion_items = $('.accordion');

                    if (accordion_items.length <= 0) {
                        return false;
                    }

                    accordion_items.each(function () {
                        var $this = $(this);

                        $this.find('.entry-title a').on('click', function (e) {
                            $this.toggleClass('open');
                            e.preventDefault();
                        });
                    });
                }
                ,
                maps: function () {
                    var map_id = '#map',
                        $map = $(map_id),
                        marker = {},
                        map = null,
                        center_map = true, // Google Maps object
                        show_clusters = $(map_id).hasClass('cluster-map'),
                        show_all_entries = $(map_id).hasClass('all-entries-map');

                    if ($map.length <= 0 || typeof schools === 'undefined') {
                        // console.info('No latitude / longitude defined!');
                        return false;
                    }
                    var instrumentMarker = [];
                    var singenMarker = [];
                    var tanzenMarker = [];
                    var koopMarker = [];

                    markers = window.schools;
                    var map_config = {
                        div: map_id,
                        zoom: 14,
                        scrollwheel: false
                    };

                    if (show_all_entries) {
                        map_config.zoom = 8;
                    }


                    if (!show_clusters && typeof markers[0].lat !== 'undefined' && typeof markers[0].lng !== 'undefined') {
                        map_config.lat = markers[0].lat;
                        map_config.lng = markers[0].lng;

                        map_config.lat = '51.5677489';
                        map_config.lng = '7.26657439999999';

                        center_map = false;
                    }
                    else {
                        map_config.lat = '51.5677489';
                        map_config.lng = '7.26657439999999';
                    }

                    if (show_clusters) {
                        map_config.markerClusterer = function (map) {
                            options = {
                                gridSize: 75,
                                maxZoom: 16,
                                styles: [
                                    {
                                        url: '/app/themes/jekits/dist/img/cluster_blau.png',
                                        width: 100,
                                        height: 100,
                                        // anchorIcon: [50, 50],
                                        textColor: '#ffffff',
                                        textSize: 10
                                    }
                                ]
                            };

                            return new MarkerClusterer(map, [], options);
                        }
                    }

                    map = new GMaps(map_config);

                    $.each(markers, function (key, value) {
                        var mark = value,
                            i = key;

                        var pinColor = mark.marker;
                        var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                            new google.maps.Size(21, 34),
                            new google.maps.Point(0, 0),
                            new google.maps.Point(10, 34)
                        );

                        // Create new Marker and push it to the repsonding array,
                        // based on school_type.
                        if (mark.type === 'address') {
                            GMaps.geocode({
                                address: mark.address,
                                callback: function (results, status) {
                                    if (status === 'OK') {
                                        var latlng = results[0].geometry.location;
                                        if (center_map) {
                                            map.setCenter(latlng.lat(), latlng.lng());
                                            center_map = false;
                                        }

                                        var marker = map.addMarker({
                                            lat: latlng.lat(),
                                            lng: latlng.lng(),
                                            title: mark.name,
                                            icon: pinImage,
                                            infoWindow: {
                                                content: '<b>' + mark.name + '</b><br />' + mark.address
                                            }
                                        });

                                        if (mark.school_type === 'instrumente') {
                                            instrumentMarker.push(marker);
                                        }

                                        if (mark.school_type === 'tanzen') {
                                            tanzenMarker.push(marker);
                                        }

                                        if (mark.school_type === 'singen') {
                                            singenMarker.push(marker);
                                        }

                                        if (mark.school_type === 'koop') {
                                            koopMarker.push(marker);
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            var marker = map.addMarker({
                                lat: mark.lat,
                                lng: mark.lng,
                                icon: pinImage,
                                animation: google.maps.Animation.DROP,
                                infoWindow: {
                                    content: '<b>' + mark.name + '</b><br />' + mark.address
                                }
                            });

                            if (mark.school_type === 'instrumente') {
                                instrumentMarker.push(marker);
                            }

                            if (mark.school_type === 'tanzen') {
                                tanzenMarker.push(marker);
                            }

                            if (mark.school_type === 'singen') {
                                singenMarker.push(marker);
                            }

                            if (mark.school_type === 'koop') {
                                koopMarker.push(marker);
                            }
                        }
                    });


                    /**
                     * Toogle Marker on map based on their type (instrumente, tanzen, singen).
                     * And the second parameter indicates if it should get shown or hidden.
                     *
                     * @param  Array type instrumente, tanzen, singen
                     * @param  bool show true, false
                     * @return {[type]}      [description]
                     */
                    Roots.common.maps.toggleMarker = function (type, show) {
                        var currentType;
                        console.log("show marker");

                        if (type === 'instrumente') {
                            currentType = instrumentMarker;
                        }

                        if (type === 'singen') {
                            currentType = singenMarker;
                        }

                        if (type === 'tanzen') {
                            currentType = tanzenMarker;
                        }

                        if (type === 'koop') {
                            currentType = koopMarker;
                        }

                        if (type === "") {
                            currentType = instrumentMarker.concat(tanzenMarker, singenMarker, koopMarker);
                        }

                        $.each(currentType, function () {
                            if (show === true) {
                                this.setVisible(true);
                            } else {
                                this.setVisible(false);
                            }

                        });
                    };
                }

            },
// Home page
            home: {
                init: function () {
                    // JavaScript to be fired on the home page
                }
            }
            ,
// About us page, note the change from about-us to about_us.
            about_us: {
                init: function () {
                    // JavaScript to be fired on the about us page
                }
            }
        }
    ;

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var namespace = Roots;
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            UTIL.fire('common');

            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
            });
        }
    };

    $(document).ready(UTIL.loadEvents);

})
(jQuery); // Fully reference jQuery after this point.
>>>>>>> origin/relaunch2018

