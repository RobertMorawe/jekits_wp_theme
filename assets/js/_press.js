
jQuery(document).ready(function($){

	var load_button = $('#load-more'),
			press_wrapper = $('.press');

	if ( press_wrapper.length > 0 && load_button.length > 0 ) {


		press_wrapper.find('.press-item').each(function(index, value) {
			if ( index < 5 ) {
				$(this).show();
			}
		});

		load_button.on('click', function(e) {
			e.preventDefault();
			press_wrapper.find('.press-item').show();
			load_button.addClass('disabled');
		});
	}

});