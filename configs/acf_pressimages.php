<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_54198fd89f5fe',
	'title' => 'Pressefotos',
	'fields' => array (
		array (
			'key' => 'field_54198ff178893',
			'label' => 'Datum',
			'name' => 'date',
			'prefix' => '',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'display_format' => 'd.m.Y',
			'return_format' => 'd.m.Y',
			'first_day' => 1,
		),
		array (
			'key' => 'field_5419900578894',
			'label' => 'Pressefotos',
			'name' => 'pressefotos',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Eintrag hinzufügen',
			'sub_fields' => array (
				array (
					'key' => 'field_5419937f77c90',
					'label' => 'Foto',
					'name' => 'image',
					'prefix' => '',
					'type' => 'image',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'column_width' => '',
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'uploadedTo',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'press_images',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'permalink',
		1 => 'the_content',
		2 => 'excerpt',
		3 => 'custom_fields',
		4 => 'discussion',
		5 => 'comments',
		6 => 'slug',
		7 => 'author',
		8 => 'format',
		9 => 'page_attributes',
		10 => 'featured_image',
		11 => 'categories',
		12 => 'tags',
		13 => 'send-trackbacks',
	),
));

endif;