<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_53ea0fe5c5771',
	'title' => 'Gallery',
	'fields' => array (
		array (
			'key' => 'field_53ea101a804a4',
			'label' => 'Galerie',
			'name' => 'galerie',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array (
				array (
					'key' => 'field_53ea1105474c0',
					'label' => 'Galerie Bild',
					'name' => 'galerie_bild',
					'prefix' => '',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'column_width' => '',
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-gallerie',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;
 ?>