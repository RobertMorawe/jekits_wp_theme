<?php

// require_once 'acf_startseite.php';


//require_once 'acf_stoerer.php';
//require_once 'acf_textseite.php';
//require_once 'acf_gallery.php';
//require_once 'acf_fortbildung.php';
//require_once 'acf_inhaltselemente.php';
//require_once 'acf_newsletter.php';
//require_once 'acf_press.php';
//require_once 'acf_pressimages.php';
//require_once 'acf_option_fortbildung.php';
//require_once 'acf_option_interner_bereich.php';
//require_once 'acf_accordion.php';
//require_once 'acf_footer.php';

if( function_exists('acf_add_options_page') ) {
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Inhalte & Einstellungen',
		'menu_title' 	=> 'Inhalte & Einstellungen',
		'menu_slug' 	=> 'static-content-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}

if( function_exists('acf_add_options_sub_page') ){
	acf_add_options_sub_page(array(
		'title' => 'Störer',
		'parent' => 'static-content-settings',
		'capability' => 'edit_posts'
	));

//	acf_add_options_sub_page(array(
//		'title' => 'Gallerie',
//		'parent' => 'static-content-settings',
//		'capability' => 'edit_posts'
//	));

//	acf_add_options_sub_page(array(
//		'title' => 'Footer',
//		'parent' => 'static-content-settings',
//		'capability' => 'edit_posts'
//	));

//	acf_add_options_sub_page(array(
//		'title' => 'Newsletter',
//		'parent' => 'static-content-settings',
//		'capability' => 'edit_posts'
//	));

	acf_add_options_sub_page(array(
		'title' => 'Fortbildung',
		'parent' => 'static-content-settings',
		'capability' => 'edit_posts'
	));

//	acf_add_options_sub_page(array(
//		'title' => 'Interner Bereich',
//		'parent' => 'static-content-settings',
//		'capability' => 'edit_posts'
//	));
}