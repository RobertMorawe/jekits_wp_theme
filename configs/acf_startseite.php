<?php

if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_53e4843b3a63a',
	'title' => 'Startseite',
	'fields' => array (
		array (
			'key' => 'field_53e4848bdcabf',
			'label' => 'Einleitungstext',
			'name' => 'intro_text',
			'prefix' => '',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'default_value' => 'Gusen fertelman Klamm geragen, hund Freserling hussental men stark verbasen. Linden persch trupplin Frausen querscheid missentang Vertranen bind.',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 3,
			'new_lines' => 'br',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53e4a61a6edff',
			'label' => 'Sartseiten-Bild',
			'name' => 'home_image',
			'prefix' => '',
			'type' => 'image',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'return_format' => 'id',
			'preview_size' => 'medium',
			'library' => 'all',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-allgemein',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;