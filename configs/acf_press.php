<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_54198f3b4bdbd',
	'title' => 'Presse',
	'fields' => array (
		array (
			'key' => 'field_54198f69e138c',
			'label' => 'Datum',
			'name' => 'date',
			'prefix' => '',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'display_format' => 'd.m.Y',
			'return_format' => 'd.m.Y',
			'first_day' => 1,
		),
		array (
			'key' => 'field_54198fa6e138d',
			'label' => 'Pressetext als PDF',
			'name' => 'press_pdf',
			'prefix' => '',
			'type' => 'file',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'return_format' => 'url',
			'library' => 'uploadedTo',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'press',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'permalink',
		1 => 'the_content',
		2 => 'excerpt',
		3 => 'custom_fields',
		4 => 'discussion',
		5 => 'comments',
		6 => 'slug',
		7 => 'author',
		8 => 'format',
		9 => 'page_attributes',
		10 => 'featured_image',
		11 => 'categories',
		12 => 'tags',
		13 => 'send-trackbacks',
	),
));

endif;