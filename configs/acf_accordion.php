<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_5475b449c9fd7',
	'title' => 'Accordion',
	'fields' => array (
		array (
			'key' => 'field_5475b45a05c8b',
			'label' => 'Accordion-Felder',
			'name' => 'accordion_fields',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'min' => '',
			'max' => '',
			'layout' => 'row',
			'button_label' => 'Eintrag hinzufügen',
			'sub_fields' => array (
				array (
					'key' => 'field_5475b47605c8c',
					'label' => 'Titel',
					'name' => 'title',
					'prefix' => '',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'column_width' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_5475b48505c8d',
					'label' => 'Inhalt',
					'name' => 'content',
					'prefix' => '',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'column_width' => '',
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'basic',
					'media_upload' => 1,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;