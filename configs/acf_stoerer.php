<?php
if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_5405cef755817',
	'title' => 'Störer',
	'fields' => array (
		array (
			'key' => 'field_5405cf07fd550',
			'label' => 'Status',
			'name' => 'status',
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => 'Soll der Störer aktiv geschaltet werden?',
			'required' => 0,
			'conditional_logic' => 0,
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5405cf36fd551',
			'label' => 'Titel',
			'name' => 'titel',
			'prefix' => '',
			'type' => 'text',
			'instructions' => 'Titel des Störers',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5405cf07fd550',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5405cf4efd552',
			'label' => 'Beschreibung',
			'name' => 'beschreibung',
			'prefix' => '',
			'type' => 'textarea',
			'instructions' => 'Inhalt des Störers',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5405cf07fd550',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => 'wpautop',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5405cf64fd553',
			'label' => 'Verlinkung',
			'name' => 'verlinkung',
			'prefix' => '',
			'type' => 'page_link',
			'instructions' => 'Verlinkung zu mehr Informationen',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5405cf07fd550',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'post_type' => '',
			'taxonomy' => '',
			'allow_null' => 0,
			'multiple' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-stoerer',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;
 ?>