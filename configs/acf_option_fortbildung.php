<?php

if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_541af7dc9eb80',
	'title' => 'Option - Fortbildung',
	'fields' => array (
		array (
			'key' => 'field_541af7ed542dd',
			'label' => 'Seite mit dem Anmeldeformular',
			'name' => 'form_page_id',
			'prefix' => '',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'post_type' => array (
				0 => 'page',
			),
			'taxonomy' => '',
			'allow_null' => 0,
			'multiple' => 0,
			'return_format' => 'id',
			'ui' => 1,
		),
		array (
			'key' => 'field_54c67b368bc8a',
			'label' => 'Seite mit dem Downloads',
			'name' => 'page_id_download',
			'prefix' => '',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'post_type' => array (
				0 => 'page',
			),
			'taxonomy' => '',
			'allow_null' => 0,
			'multiple' => 0,
			'return_format' => 'id',
			'ui' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-fortbildung',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;