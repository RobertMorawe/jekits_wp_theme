<?php

if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_53e4bb1e076de',
	'title' => 'Footer',
	'fields' => array (
		array (
			'key' => 'field_53e4bb24a7601',
			'label' => 'Informations Text',
			'name' => 'footer_outro_text',
			'prefix' => '',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'default_value' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 4,
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53e4bbc7a7602',
			'label' => 'Verlinkung',
			'name' => 'footer_info_link',
			'prefix' => '',
			'type' => 'page_link',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'post_type' => '',
			'taxonomy' => '',
			'allow_null' => 0,
			'multiple' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-footer',
			),
		),
	),
	'menu_order' => 99,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;