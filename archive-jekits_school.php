<?php get_template_part('templates/neighbourhood-search'); ?>
<section class="main-content school-map">
	<?php
		$zip_query = $term_query = $meta_query = $coordinates = array();
		global $wp_query;

		wp_reset_postdata();

		if ( get_current_zip() !== '' ) {
			$zip_query = array(
				'key' => 'zip',
				'value' => get_current_zip(),
				'compare' => 'LIKE'
			);
		}

		if ( get_current_term() !== '' ) {

			$key = 'stadt_kommune';
			if ( $_POST['type'] === 'jeki')
			{
				$key = 'city';
			}
			$term_query = array(
				'key' => $key,
				'value' => get_current_term(),
				'compare' => 'LIKE'
			);
		}

		if ( get_current_term() !== '' && get_current_zip() !== '') {
			$meta_query = array(
				'relation' => 'AND',
				$zip_query,
				$term_query
			);
		}
		else if ( get_current_term() !== '' ) {
			$meta_query = array(
				$term_query
			);
		}
		else {
			$meta_query = array(
				$zip_query
			);
		}

		$postType = 'jekits_school';

		if ( isset($_POST['type']) && $_POST['type'] === 'jeki')
		{
			$postType = 'school';
		}

		$query = new WP_Query(array(
			'post_type' => $postType,
			'numberposts' => -1,
			'posts_per_page' => -1,
			'post_title_like' => get_current_term(),
			'orderby' => 'title',
			'order' => 'ASC',
			'meta_query' => $meta_query
		));

		$search_term = get_current_term();
		if ( empty($search_term) ) {
			$search_term = get_current_zip();
		}
		else if (get_current_term() !== '' && get_current_zip() !== '') {
			$search_term = get_current_zip() . ', ' . get_current_term();
		}
	?>

	<div class="school-list">
		<h2>Suchergebnisse für "<?php echo $search_term; ?>"</h2>
		<?php if($postType === 'jekits_school'): ?>
			<?php require_once('partials/jekits-school-filter.php'); ?>
		<?php elseif($postType === 'school'): ?>
			<?php require_once('partials/school.php'); ?>
		<?php endif; ?>

	</div>
	<div class="map">
		<div id="map" class="cluster-map all-entries-map" style="width: 100%; height: 100%;"></div>
		<?php if (!empty($coordinates)): ?>
		<script>
			var schools = <?php echo json_encode($coordinates); ?>;
		</script>
		<?php endif; ?>
	</div>
</section>
<?php wp_reset_query(); ?>
