<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<div class="page-header">
					<h1>404 - Seite nicht gefunden.</h1>
					<h3 class="center"><?php _e('Die Seite, die Sie suchen, wurde nicht gefunden.', 'roots'); ?></h3>
				</div>

				<p style="text-align:center;"><b><?php _e('Folgende Fehler könnten dazu geführt haben:', 'roots'); ?></b></p>
				<p style="text-align:center;">
					<?php _e('eine falsch geschriebene Adresse', 'roots'); ?><br />
					<?php _e('ein veralteter Link', 'roots'); ?>
				</p>
			</div>
		</div>
	</div>
</section>