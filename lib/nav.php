<?php

/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * Roots_Nav_Walker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 */
class Roots_Nav_Walker extends Walker_Nav_Menu
{
    function check_current($classes)
    {
        return preg_match('/(current[-_])|active|dropdown/', $classes);
    }

    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $output .= "\n<ul class=\"dropdown-menu\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $item_html = '';
        parent::start_el($item_html, $item, $depth, $args);

        if ($item->is_dropdown && ($depth === 0)) {
            $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
            $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
        } elseif (stristr($item_html, 'li class="divider')) {
            $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
        } elseif (stristr($item_html, 'li class="dropdown-header')) {
            $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
        }

        $item_html = apply_filters('roots/wp_nav_menu_item', $item_html);
        $output .= $item_html;
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

        if ($element->is_dropdown) {
            $element->classes[] = 'dropdown';
        }

        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}

/**
 * Remove the id="" on nav menu items
 * Return 'menu-slug' for nav menu classes
 */
function roots_nav_menu_css_class($classes, $item)
{
    $slug = sanitize_title($item->title);
    $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
    $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

    $classes[] = 'menu-' . $slug;

    $classes = array_unique($classes);

    return array_filter($classes, 'is_element_empty');
}

add_filter('nav_menu_css_class', 'roots_nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', '__return_null');

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Use Roots_Nav_Walker() by default
 */
function roots_nav_menu_args($args = '')
{
    $roots_nav_menu_args['container'] = false;

    if (!$args['items_wrap']) {
        $roots_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
    }

    if (!$args['depth']) {
        $roots_nav_menu_args['depth'] = 3;
    }

    if (!$args['walker']) {
        $roots_nav_menu_args['walker'] = new Roots_Nav_Walker();
    }

    return array_merge($args, $roots_nav_menu_args);
}

add_filter('wp_nav_menu_args', 'roots_nav_menu_args');

/**
 * Add Search & Login Button
 */
//add_filter('wp_nav_menu_items', 'add_search_box_to_menu', 10, 2);
function add_search_box_to_menu($items, $args)
{
    if ($args->theme_location == 'primary_navigation')
        return $items . "<li class='btn-nav icon-search'> <a href='#'>Suche</a></li>";

    return $items;
}

add_filter('wp_nav_menu_items', 'add_loginout_link', 10, 2);
function add_loginout_link($items, $args)
{

    if ($args->theme_location == 'primary_navigation') {


            $items .= '<li class="btn-nav icon-login"><a href="#">Login</a>';

        if (is_user_logged_in() ) {
//            $items .= '<li class="btn-nav icon-login"><a href="' . wp_logout_url() . '">Login</a>';
        } else {
//            $items .= '<li class="btn-nav icon-login"><a href="' . get_permalink(get_field('internal_page_id', 'options')) . '">Login</a>';
        }
        $items .= '<ul class="dropdown-menu dropdown-menu-left">';
        $items .= '<li class=""><a href="http://intranet.jekits.de">Intranet</a></li>';
        $items .= '<li class=""><a href="http://materialpool.jekits.de">Materialpool</a></li>';

        $items .= '</ul>';
        $items .= '</li>';

    }

    return $items;
}

add_filter('wp_nav_menu_items', 'add_home_icon', 1, 2);
function add_home_icon($items, $args)
{

<<<<<<< HEAD
    $items = '
		<li class="btn-nav icon-home active menu-home"><a href="/">Home</a></li>
		<li ' . $class . '>
			<a data-toggle="dropdown" data-target="#" href="#">Das Programm</a>
			<div class="programm">
				<div id="collapse-mainmenu" class="collapse-menu" role="tablist" aria-multiselectable="true">
					<div class="panel">
						' . $jekits_markup . '
					</div>
				</div>
				<div class="neighbourhood-search">
					<h2>JeKits in meiner Nähe</h2>
					<form method="post" action="' . get_posttype_slug() . '">
                <input type="hidden" name="type" value="jekits">
						<input class="zip" type="text" name="zip" placeholder="Postleitzahl" value="' . get_current_zip() . '" />
						<input class="name_place" type="text" name="term" placeholder="Ort oder Name der Schule" value="' . get_current_term() . '" />
						<p class="neighbourhood-search-data-privacy">
						Durch Anklicken der unten stehenden Checkbox und das Anklicken der Schaltfläche „Suchen“ erklären Sie sich damit einverstanden, dass die hier eingegebenen Daten zusammen mit ihrer IP-Adresse verarbeitet und an den Anbieter des Kartendienstes (Google USA) weiterleiten. Die Verarbeitung erfolgt ausschließlich für den Zweck der Nutzung des Angebots „JeKits in meiner Nähe“. Wenn Sie hiermit nicht einverstanden sind, ist eine Nutzung der Funktion „JeKits in meiner Nähe“ leider nicht möglich.
                        </p>
                         <p>
                        <input onchange="this.setCustomValidity(validity.valueMissing ? \'Bitte akzeptieren Sie die Datenschutzerklärung.\' : \'\');" required type="checkbox" id="checkbox-privatepolicy" name="google-search" value="search">
                         <label class="plz-search" for="google-search">Ich bin damit einverstanden.</label>
                        </p>
						<input class="hide no-refresh" type="submit" value="suchen"/>
					</form>
					
				</div>
			</div>
		</li>' . $items;
=======
    $items = '<li class="btn-nav icon-home active menu-home"><a href="/">Home</a></li>' . $items;
>>>>>>> origin/relaunch2018

    return $items;
}

