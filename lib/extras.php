<<<<<<< HEAD
<?php

require_once('flei_wp_toolkit.php');

/**
 * Remove unnecessary code from WordPress header
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

add_action( 'widgets_init', 'my_remove_recent_comments_style' );
function my_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'  ) );
}

// add $cap capability to this role object
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );


/**
 * Diable lost password feature
 */
function disable_reset_lost_password() {
	return false;
}
add_filter( 'allow_password_reset', 'disable_reset_lost_password');

function remove_lost_your_password($text) {
	$text = str_replace( 'Passwort vergessen?', '', $text );
	$text = str_replace( 'https://wordpress.org/', '/', $text );
	$text = str_replace( 'Powered by WordPress', 'Zurück zur Startseite', $text );

	return $text;
}
add_filter( 'gettext', 'remove_lost_your_password'  );

function my_login_logo() { ?>
    <style type="text/css">
   			 @font-face {
					font-family: 'Typewriter';
					src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.eot');
					src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.svg') format('svg');
					font-weight: normal;
					font-style: normal;
				}

				@font-face {
					font-family: 'Grotesk';
					src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.eot');
					src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.svg') format('svg');
					font-weight: normal;
					font-style: normal;
				}

				@font-face {
					font-family: 'Grotesk';
					src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.eot');
					src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.svg') format('svg');
					font-weight: 100;
					font-style: normal;
				}
		    body {
		    	background-color: #fff;
		    	font-family: 'Grotesk', helvetica, sans-serif;
		    	font-weight: 100;
		    }
		    input[type="text"], input[type="password"]{
					font-weight: 100;
				}
		    .login form {
					background: rgba(0, 166, 235, 0.02);
					padding: 26px 24px 30px;
				}
				#wp-submit {
					display: block;
					width: 100%;
					margin-top: 20px;
					background: #00a6eb;
					border-radius: 5px;
					border: 0;
					font-size: 15px;
					line-height: 15px;
					font-family: 'Typewriter', helvetica, sans-serif;
					font-weight: 400;
					padding: 17px 21px;
					height: auto;
					box-shadow: none;
					-webkit-box-shadow: none;
					-webkit-transition: background-color 0.2s linear;
					-o-transition: background-color 0.2s linear;
					transition: background-color 0.2s linear;
				}

				#wp-submit:hover, #wp-submit:focus {
					background-color: #00709f;
				}
        body.login div#login h1 a {
            background: transparent url('<?php echo get_stylesheet_directory_uri(); ?>/dist/img/brand-logo.png') center center no-repeat;
						padding-bottom: 30px;
						width: 100%;
						padding: 0;
						margin: 0;
						height: 114px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) || in_array( 'editor', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			show_admin_bar(false);
			return get_permalink(get_field('internal_page_id', 'options'));
		}
	} else {
		return $redirect_to;
	}
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

if ( !current_user_can('edit_posts') ) {
	add_filter( 'show_admin_bar', '__return_false' );
}

function redirect_after_logout() {
	if ( !current_user_can('edit_posts') ) {
		wp_redirect( get_permalink(get_field('internal_page_id', 'options')) );
		exit;
	}
}
add_action('wp_logout', 'redirect_after_logout');


/**
 * Returns a formatted date
 */
function get_merged_date($from = false, $to = false) {
	$date_output = $from;

	if ( !empty($to) ) {
		$date_from_timestamp = strtotime($from);
		$date_from_day = date('d.', $date_from_timestamp);
		$date_output = $date_from_day . '/' . $to;
	}

	return $date_output;
}

/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more($more) {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
//add_filter('excerpt_more', 'roots_excerpt_more');

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Manage output of wp_title()
 */
function roots_wp_title($title) {
  if (is_feed()) {
    return $title;
  }

  $title .= get_bloginfo('name');

  return $title;
}
add_filter('wp_title', 'roots_wp_title', 10);

/**
 * Fortbildungs Posttype
 */
function post_type_fortbildung() {
	register_post_type(
		'fortbildung',
		array(
			'label' => __('Fortbildungen'),
			'public' => true,
			'show_ui' => true,
			'menu_icon' => '',
			'supports' => array(
				'title',
				'revisions',
				'editor',
				'excerpt'
			)
		)
	);

	register_taxonomy( 'fortbildung_category', 'fortbildung',
		array(
			'show_ui' => true,
			'show_tagcloud' => false,
			'hierarchical' => true,
			'label' => __('Kategorie'),
			'query_var' => 'kategorie',
			'rewrite' => array('slug' => __('bereich', 'roots') )
		)
	);
}
add_action('init', 'post_type_fortbildung');

add_filter('manage_fortbildung_posts_columns', 'my_fortbildung_table_head');
function my_fortbildung_table_head( $columns ) {

    $columns['fort_kategorie']  = 'Kategorie';

    return $columns;

}
add_action( 'manage_fortbildung_posts_columns', 'my_fortbildung_table_head', 10, 2 );

function my_fortbildung_table_content( $column_name, $post_id ) {

    if( $column_name == 'fort_kategorie' ) {
        $fort_category = get_field('kategorie', $post->ID);
        echo $fort_category;
      }
}

function flex_content_elements($column = 'column_1') {
	while(the_flexible_field($column)) {
		if(get_row_layout() == "image") {
			$image = get_sub_field("image");
			$image_src = $image['sizes']['medium'];
			$image_title = $image['title'];
			$image_alt = $image['alt'];
			echo <<<EOF
    		<img src="$image_src" title="$image_title" alt="$image_alt" />
EOF;

		}
		elseif (get_row_layout() == "text") {
			echo get_sub_field("text");
		}
		elseif (get_row_layout() == "button") {
			$target = "";
			$button_text = get_sub_field("button_text");
			$button_link = get_sub_field("button_link");
			$button_link_external = get_sub_field("button_link_external");
			if ( !empty($button_link_external) ) {
				$button_link = $button_link_external;
				$target = ' target="_blank"';
			}
			echo <<<EOF
    		<a class="btn btn-dark" href="$button_link"$target>$button_text</a>
EOF;
		}
	}
}

/**
 * POST-TYPE - Press
 */
function post_type_press() {
	register_post_type(
		'press',
		array(
			'label' => __('Pressetexte'),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => '',
			'exclude_from_search' => true,
			'supports' => array(
				'title'
			)
		)
	);
}
add_action('init', 'post_type_press');

/**
 * POST-TYPE - Press
 */
function post_type_press_images() {
	register_post_type(
		'press_images',
		array(
			'label' => __('Pressefotos'),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => '',
			'exclude_from_search' => true,
			'supports' => array(
				'title'
			)
		)
	);
}
add_action('init', 'post_type_press_images');

/**
 * POST-TYPE - Musikschule
 */
function post_type_musicschool() {
	register_post_type(
		'musicschool',
		array(
			'label' => __('Musikschulen'),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => '',
			'exclude_from_search' => true,
			'supports' => array(
				'title',
				'revisions',
				'editor'
			)
		)
	);

	register_taxonomy( 'musicschool_category', 'musicschool',
		array(
			'show_ui' => true,
			'show_tagcloud' => false,
			'hierarchical' => true,
			'label' => __('Stadt'),
			'query_var' => 'kategorie',
			'rewrite' => array('slug' => __('bereich', 'roots') )
		)
	);
}
add_action('init', 'post_type_musicschool');

function add_menu_icons_styles() {
?>
	<style>
		#adminmenu .menu-icon-press div.wp-menu-image:before { content: "\f123"; }
		#adminmenu .menu-icon-school div.wp-menu-image:before { content: "\f230"; }
		#adminmenu .menu-icon-fortbildung div.wp-menu-image:before { content: "\f118"; }
		#adminmenu .menu-icon-press_images div.wp-menu-image:before { content: "\f161"; }
		#adminmenu .menu-icon-musicschool div.wp-menu-image:before { content: "\f127"; }
	</style>
	<?php
}
add_action( 'admin_head', 'add_menu_icons_styles' );


/**
 * POST-TYPE - Musikschule
 */
// function post_type_musicschool() {
// 	register_post_type(
// 		'musicschool',
// 		array(
// 			'label' => __('Musikschulen'),
// 			'public' => true,
// 			'has_archive' => false,
// 			'menu_icon' => '',
// 			'exclude_from_search' => true,
// 			'supports' => array(
// 				'title',
// 				'revisions',
// 				'editor'
// 			)
// 		)
// 	);

// 	register_taxonomy( 'musicschool_category', 'musicschool',
// 		array(
// 			'show_ui' => true,
// 			'show_tagcloud' => false,
// 			'hierarchical' => true,
// 			'label' => __('Stadt'),
// 			'query_var' => 'kategorie',
// 			'rewrite' => array('slug' => __('bereich', 'roots') )
// 		)
// 	);
// }
// add_action('init', 'post_type_musicschool');


function my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
						<p>Dieser Inhalt ist passwortgeschützt. Um ihn anzuschauen, geben Sie bitte Ihr Passwort unten ein:</p>
						<p><label for="pwbox-1530">Passwort: <input name="post_password" id="pwbox-1530" type="password" size="20"></label> <input type="submit" name="Submit" value="Anmelden"></p>
					</form>';
    return $o;
}

function blank($title) {
	return '%s';
}

add_filter( 'the_password_form', 'my_password_form' );
add_filter('protected_title_format', 'blank');


/**
 * Custom rewrites for fortbildung
 */
function prefix_fortbildung_rewrite_rule() {
	add_rewrite_rule( 'fortbildung/([^/]+)/bestaetigung', 'index.php?fortbildung=$matches[1]&agree=yes', 'top' );
}

function prefix_register_query_var( $vars ) {
	$vars[] = 'agree';
	return $vars;
}

function prefix_url_rewrite_templates() {

	if ( get_query_var( 'agree' ) && is_singular( 'fortbildung' ) ) {
		add_filter( 'template_include', function() {
			return get_template_directory() . '/single-fortbildung-agree.php';
		});
	}

}

add_action( 'init', 'prefix_fortbildung_rewrite_rule' );
add_filter( 'query_vars', 'prefix_register_query_var' );
add_action( 'template_redirect', 'prefix_url_rewrite_templates' );
=======
<?php

/**
 * Remove unnecessary code from WordPress header
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

add_action('widgets_init', 'my_remove_recent_comments_style');
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

// add $cap capability to this role object
$role_object = get_role('editor');
$role_object->add_cap('edit_theme_options');

/**
 * Diable lost password feature
 */
function disable_reset_lost_password()
{
    return false;
}

add_filter('allow_password_reset', 'disable_reset_lost_password');

function remove_lost_your_password($text)
{
    $text = str_replace('Passwort vergessen?', '', $text);
    $text = str_replace('https://wordpress.org/', '/', $text);
    $text = str_replace('Powered by WordPress', 'Zurück zur Startseite', $text);

    return $text;
}

add_filter('gettext', 'remove_lost_your_password');

function my_login_logo()
{ ?>
    <style type="text/css">
        @font-face {
            font-family: 'Typewriter';
            src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.eot');
            src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/TypewriterURW-Med.svg') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'Grotesk';
            src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.eot');
            src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Med.svg') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'Grotesk';
            src: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.eot');
            src: local('☺'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.ttf') format('truetype'), url('<?php echo get_stylesheet_directory_uri(); ?>/dist/fonts/URWGrotesk-Lig.svg') format('svg');
            font-weight: 100;
            font-style: normal;
        }

        body {
            background-color: #fff;
            font-family: 'Grotesk', helvetica, sans-serif;
            font-weight: 100;
        }

        input[type="text"], input[type="password"] {
            font-weight: 100;
        }

        .login form {
            background: rgba(0, 166, 235, 0.02);
            padding: 26px 24px 30px;
        }

        #wp-submit {
            display: block;
            width: 100%;
            margin-top: 20px;
            background: #00a6eb;
            border-radius: 5px;
            border: 0;
            font-size: 15px;
            line-height: 15px;
            font-family: 'Typewriter', helvetica, sans-serif;
            font-weight: 400;
            padding: 17px 21px;
            height: auto;
            box-shadow: none;
            -webkit-box-shadow: none;
            -webkit-transition: background-color 0.2s linear;
            -o-transition: background-color 0.2s linear;
            transition: background-color 0.2s linear;
        }

        #wp-submit:hover, #wp-submit:focus {
            background-color: #00709f;
        }

        body.login div#login h1 a {
            background: transparent url('<?php echo get_stylesheet_directory_uri(); ?>/dist/img/brand-logo.png') center center no-repeat;
            padding-bottom: 30px;
            width: 100%;
            padding: 0;
            margin: 0;
            height: 114px;
        }
    </style>
<?php }

add_action('login_enqueue_scripts', 'my_login_logo');

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect($redirect_to, $request, $user)
{
    //is there a user to check?
    global $user;
    if (isset($user->roles) && is_array($user->roles)) {
        //check for admins
        if (in_array('administrator', $user->roles) || in_array('editor', $user->roles)) {
            // redirect them to the default place
            return $redirect_to;
        } else {
            show_admin_bar(false);

            return get_permalink(get_field('internal_page_id', 'options'));
        }
    } else {
        return $redirect_to;
    }
}

add_filter('login_redirect', 'my_login_redirect', 10, 3);

if (!current_user_can('edit_posts')) {
    add_filter('show_admin_bar', '__return_false');
}

function redirect_after_logout()
{
    if (!current_user_can('edit_posts')) {
        wp_redirect(get_permalink(get_field('internal_page_id', 'options')));
        exit;
    }
}

add_action('wp_logout', 'redirect_after_logout');

/**
 * Returns a formatted date
 */
function get_merged_date($from = false, $to = false)
{
    $date_output = $from;

    if (!empty($to)) {
        $date_from_timestamp = strtotime($from);
        $date_from_day = date('d.', $date_from_timestamp);
        $date_output = $date_from_day . '/' . $to;
    }

    return $date_output;
}

/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more($more)
{
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}

//add_filter('excerpt_more', 'roots_excerpt_more');

function custom_excerpt_length($length)
{
    return 20;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function custom_excerpt_more($more)
{
    return '...';
}

add_filter('excerpt_more', 'custom_excerpt_more');

/**
 * Manage output of wp_title()
 */
function roots_wp_title($title)
{
    if (is_feed()) {
        return $title;
    }

    $title .= get_bloginfo('name');

    return $title;
}

add_filter('wp_title', 'roots_wp_title', 10);

/**
 * Fortbildungs Posttype
 */
function post_type_fortbildung()
{
    register_post_type(
        'fortbildung',
        array(
            'label'     => __('Fortbildungen'),
            'public'    => true,
            'show_ui'   => true,
            'menu_icon' => '',
            'supports'  => array(
                'title',
                'revisions',
                'editor',
                'excerpt',
            ),
        )
    );

    register_taxonomy('fortbildung_category', 'fortbildung',
        array(
            'show_ui'       => true,
            'show_tagcloud' => false,
            'hierarchical'  => true,
            'label'         => __('Kategorie'),
            'query_var'     => 'kategorie',
            'rewrite'       => array('slug' => __('bereich', 'roots')),
        )
    );
}

add_action('init', 'post_type_fortbildung');

add_filter('manage_fortbildung_posts_columns', 'my_fortbildung_table_head');
function my_fortbildung_table_head($columns)
{
    $new_columns = array(
        'fortbildung_category_column'   => 'Fortbildungs-Kategorie',
        'fortbildung_textauszug_column' => 'Textauszug',
    );

    return array_merge($columns, $new_columns);
}

add_action('manage_fortbildung_posts_custom_column', 'my_fortbildung_table_content', null, 2);

function my_fortbildung_table_content($column_name, $post_id)
{
    switch ($column_name) {
        case 'fortbildung_category_column':
            $terms = wp_get_post_terms($post_id, 'fortbildung_category', array('fields' => 'names'));
            echo implode(', ', $terms);
            break;
        case 'fortbildung_textauszug_column':
            echo get_the_excerpt($post_id);
            break;
    }
}

function flex_content_elements($column = 'column_1')
{
    while (the_flexible_field($column)) {
        if (get_row_layout() == "image") {
            $image = get_sub_field("image");
            $image_src = $image['sizes']['medium'];
            $image_title = $image['title'];
            $image_alt = $image['alt'];
            echo <<<EOF
    		<img src="$image_src" title="$image_title" alt="$image_alt" />
EOF;

        } elseif (get_row_layout() == "text") {
            echo get_sub_field("text");
        } elseif (get_row_layout() == "button") {
            $target = "";
            $button_text = get_sub_field("button_text");
            $button_link = get_sub_field("button_link");
            $button_link_external = get_sub_field("button_link_external");
            if (!empty($button_link_external)) {
                $button_link = $button_link_external;
                $target = ' target="_blank"';
            }
            echo <<<EOF
    		<a class="btn btn-dark" href="$button_link"$target>$button_text</a>
EOF;
        }
    }
}

/**
 * POST-TYPE - Press
 */
function post_type_press()
{
    register_post_type(
        'press',
        array(
            'label'               => __('Pressetexte'),
            'public'              => true,
            'has_archive'         => false,
            'menu_icon'           => '',
            'exclude_from_search' => true,
            'supports'            => array(
                'title',
            ),
        )
    );
}

add_action('init', 'post_type_press');

/**
 * POST-TYPE - Press
 */
function post_type_press_images()
{
    register_post_type(
        'press_images',
        array(
            'label'               => __('Pressefotos'),
            'public'              => true,
            'has_archive'         => false,
            'menu_icon'           => '',
            'exclude_from_search' => true,
            'supports'            => array(
                'title',
            ),
        )
    );
}

add_action('init', 'post_type_press_images');

/**
 * POST-TYPE - Musikschule
 */
function post_type_musicschool()
{
    register_post_type(
        'musicschool',
        array(
            'label'               => __('Musikschulen'),
            'public'              => true,
            'has_archive'         => false,
            'menu_icon'           => '',
            'exclude_from_search' => true,
            'supports'            => array(
                'title',
                'revisions',
                'editor',
            ),
        )
    );

    register_taxonomy('musicschool_category', 'musicschool',
        array(
            'show_ui'       => true,
            'show_tagcloud' => false,
            'hierarchical'  => true,
            'label'         => __('Stadt'),
            'query_var'     => 'kategorie',
            'rewrite'       => array('slug' => __('bereich', 'roots')),
        )
    );
}

//add_action('init', 'post_type_musicschool');

function add_menu_icons_styles()
{
    ?>
    <style>
        #adminmenu .menu-icon-press div.wp-menu-image:before {
            content: "\f123";
        }

        #adminmenu .menu-icon-school div.wp-menu-image:before {
            content: "\f230";
        }

        #adminmenu .menu-icon-fortbildung div.wp-menu-image:before {
            content: "\f118";
        }

        #adminmenu .menu-icon-press_images div.wp-menu-image:before {
            content: "\f161";
        }

        #adminmenu .menu-icon-musicschool div.wp-menu-image:before {
            content: "\f127";
        }
    </style>
    <?php
}

add_action('admin_head', 'add_menu_icons_styles');

/**
 * POST-TYPE - Musikschule
 */
// function post_type_musicschool() {
// 	register_post_type(
// 		'musicschool',
// 		array(
// 			'label' => __('Musikschulen'),
// 			'public' => true,
// 			'has_archive' => false,
// 			'menu_icon' => '',
// 			'exclude_from_search' => true,
// 			'supports' => array(
// 				'title',
// 				'revisions',
// 				'editor'
// 			)
// 		)
// 	);

// 	register_taxonomy( 'musicschool_category', 'musicschool',
// 		array(
// 			'show_ui' => true,
// 			'show_tagcloud' => false,
// 			'hierarchical' => true,
// 			'label' => __('Stadt'),
// 			'query_var' => 'kategorie',
// 			'rewrite' => array('slug' => __('bereich', 'roots') )
// 		)
// 	);
// }
// add_action('init', 'post_type_musicschool');

function my_password_form()
{
    global $post;
    $label = 'pwbox-' . (empty($post->ID) ? rand() : $post->ID);
    $o = '<form action="' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . '" class="post-password-form" method="post">
						<p>Dieser Inhalt ist passwortgeschützt. Um ihn anzuschauen, geben Sie bitte Ihr Passwort unten ein:</p>
						<p><label for="pwbox-1530">Passwort: <input name="post_password" id="pwbox-1530" type="password" size="20"></label> <input type="submit" name="Submit" value="Anmelden"></p>
					</form>';

    return $o;
}

function blank($title)
{
    return '%s';
}

add_filter('the_password_form', 'my_password_form');
add_filter('protected_title_format', 'blank');

/**
 * Custom rewrites for fortbildung
 */
function prefix_fortbildung_rewrite_rule()
{
    add_rewrite_rule('fortbildung/([^/]+)/bestaetigung', 'index.php?fortbildung=$matches[1]&agree=yes', 'top');
}

function prefix_register_query_var($vars)
{
    $vars[] = 'agree';

    return $vars;
}

function prefix_url_rewrite_templates()
{

    if (get_query_var('agree') && is_singular('fortbildung')) {
        add_filter('template_include', function () {
            return get_template_directory() . '/single-fortbildung-agree.php';
        });
    }

}

add_action('init', 'prefix_fortbildung_rewrite_rule');
add_filter('query_vars', 'prefix_register_query_var');
add_action('template_redirect', 'prefix_url_rewrite_templates');

add_filter('tiny_mce_before_init', 'fb_tinymce_add_pre');
function fb_tinymce_add_pre($initArray)
{

    // Comma separated string od extendes tags
    // Command separated string of extended elements
    $ext = 'svg[preserveAspectRatio|style|version|viewbox|xmlns],defs,linearGradient[id|x1|y1|z1]';

    if (isset($initArray['extended_valid_elements'])) {
        $initArray['extended_valid_elements'] .= ',' . $ext;
    } else {
        $initArray['extended_valid_elements'] = $ext;
    }
    // maybe; set tiny paramter verify_html
    $initArray['verify_html'] = false;

    return $initArray;
}

add_action('template_redirect', 'jekits_redirect_empty_form');
function jekits_redirect_empty_form()
{
    if (get_page_template_slug() == 'template-form.php' && (empty($_GET['kurs_id']) || empty($_GET['kurs_name']))) {
        wp_redirect(get_home_url());
    }
}

function jekits_fallback_excerpt($text, $raw_excerpt)
{
    if (!$raw_excerpt) {
        $content = apply_filters('the_content', get_the_content());
        $text = substr($content, 0, strpos($content, '</p>') + 4);
    }

    return $text;
}

add_filter('wp_trim_excerpt', 'jekits_fallback_excerpt', 10, 2);

add_filter('widget_posts_args', 'widget_blog_posts_only');
add_filter('widget_archives_args', 'widget_blog_posts_only');

function widget_blog_posts_only($args)
{

    $blog_terms = get_field('blog_posts_category');

    if ($blog_terms) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field'    => 'ID',
                'terms'    => $blog_terms,
            ),
        );
    }

    return $args;
}
>>>>>>> origin/relaunch2018
