<?php
/*
Template Name: Presse
*/
?>
<section class="content-header press-header" style="background-color:#<?php the_field( 'farbe' ); ?>">
	<div class="container">
		<div class="content-intro">
			<?php while ( have_rows('intro_inhalt') ) : the_row(); ?>
				<h1><?php echo get_sub_field('headline') ?></h1>
			<?php endwhile; ?>
			<div class="description">
				<p>
					Hier bieten wir Ihnen die Möglichkeit, online Informationen und Bildmaterial in Druckqualität abzurufen.<br />
                    Bitte beachten Sie, dass das Fotomaterial nur in Zusammenhang mit der Berichterstattung über das Programm „JeKits – Jedem Kind Instrumente, Tanzen, Singen“ abgedruckt werden darf. Die Bildrechte liegen bei der JeKits-Stiftung.
				</p>
                <p>
                    <b>Pressekontakt:</b><br />
                    Tanja Senicer<br />
                    Kommunikation<br /><br />
                    Telefon: 0234 54 17 47-13<br />
                    Fax: 0234 54 17 47-99<br />
                    E-Mail: <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mail/senicer-presse.png" alt="" /><br />
                    <br>
                </p>

            </div>
		</div>
	</div>
</section>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<div class="page-header">
					<h1>Aktuelle Pressemitteilungen</h1>
				</div>
				<?php
					query_posts(array(
						'post_type' => 'press',
						'posts_per_page' => -1
					));
				?>
				<div class="press">
					<?php while (	have_posts()) : the_post(); ?>
						<div class="press-item" style="display: none;">
	  					<time pubdate="pubdate"><?php the_field('date'); ?> - </time>
	  					<h2><a href="<?php the_field('press_pdf') ?>"><?php the_title(); ?></a></h2>
							<div class="download">
	  						<a href="<?php the_field('press_pdf') ?>" title="Presseinformationen downloaden" download></a>
	  					</div>
	  				</div>
					<?php endwhile; ?>
					<div class="btn-wrapper">
						<a href="#" id="load-more" class="button grey">weitere Meldungen laden</a>
					</div>
				</div>
				<?php wp_reset_query(); ?>
				<div class="page-header">
					<h1>Aktuelle Pressefotos</h1>
				</div>
				<?php
					query_posts(array(
						'post_type' => 'press_images',
						'posts_per_page' => -1
					));
				?>
				<div class="press-images">
					<?php while (	have_posts()) : the_post(); ?>
						<figure class="wp-caption alignnone">
							<?php
								$press_images = get_field('pressefotos');
								foreach ($press_images as $key => $image):
							?>
							<div class="image">
								<img src="<?php echo $image['image']['sizes']['medium']; ?>" alt="" />
								<div class="download">
		  						<a href="<?php echo $image['image']['url']; ?>" title="Pressebild downloaden" download></a>
		  					</div>
							</div>
							<?php endforeach; ?>
							<figcaption class="wp-caption-text"><span style="font-weight: 400;"><?php the_title(); ?></span><br /><time pubdate="pubdate"><?php the_field('date'); ?></time></figcaption>
						</figure>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>
</section>
