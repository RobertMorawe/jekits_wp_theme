<?php
/*
Template Name: Content
*/
?>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<?php while (have_posts()) : the_post(); ?>
  				<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<section class="callout">
	<div class="container">
		<div class="centered">
			<a class="callout-button" href="#"><span class="heading">Sie sind Interessiert?</span> <span class="cursiv">Weitere Informationen zur Programm-Bewerbung finden Sie hier </span></a>
		</div>
	</div>
</section>