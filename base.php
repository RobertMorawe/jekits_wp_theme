<?php
 //
//$groups = acf_get_local_field_groups();
//$json = [];
//
//foreach ($groups as $group) {
//    // Fetch the fields for the given group key
//    $fields = acf_get_local_fields($group['key']);
//
//    // Remove unecessary key value pair with key "ID"
//    unset($group['ID']);
//
//    // Add the fields as an array to the group
//    $group['fields'] = $fields;
//
//    // Add this group to the main array
//    $json[] = $group;
//}
//
//$json = json_encode($json, JSON_PRETTY_PRINT);
//echo "<pre>";
//echo $json;
//echo "</pre>";
//
//// Write output to file for easy import into ACF.
//// The file must be writable by the server process.
//$file = dirname(__FILE__) . '/acf-import.json';
//file_put_contents($file, $json );
//
//
?>

<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
<!--[if lt IE 8]>
<div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
</div>
<![endif]-->
<?php
do_action('get_header');
get_template_part('templates/header');
$is_video = get_page_template_slug(get_the_ID()) == 'template-video.php';
?>
<div class="wrap" role="document">
    <?php
    $style = '';
    if (has_post_thumbnail()) {
        $post_thumbnail_id = get_post_thumbnail_id();
        $post_thumbnail_uri = wp_get_attachment_image_src($post_thumbnail_id, 'fullscreen');

        $style = ' style="background-image: url(\'' . $post_thumbnail_uri[0] . '\');"';
    }
    ?>
    <?php if (!is_front_page() && !in_array('post-type-archive-school', get_body_class())):
        ?>
        <section class="content-master<?= ($is_video ? ' video' : '') ?>"<?php echo $style; ?>>
            <?php require_once('partials/language.php');

            if ($is_video):?>
                <div class="play-btn">
                    <a href="#" data-toggle="modal" data-target="#video-modal"><img src="/app/themes/jekits/dist/img/play-button.svg" alt=""></a>
                </div>
            <?php
            endif;
            ?>
        </section>
    <?php endif; ?>
    <?php if (get_field('is_introbox')): ?>
        <?php get_template_part('templates/introbox'); ?>
    <?php endif; ?>
    <?php include roots_template_path(); ?>
    <?php if (!is_page_template('template-singlepage.php')): ?>
        <?php include('templates/flex-content.php'); ?>
    <?php endif; ?>
</div><!-- /.wrap -->
<?php get_template_part('templates/footer');
if ($is_video && get_field('video_html')):?>
    <div class="modal fade video-modal" tabindex="-1" role="dialog" id="video-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <br>
                    <?=get_field('video_html')?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
</body>
</html>
