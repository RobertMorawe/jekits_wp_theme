<section class="main-content">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <div class="page-header">
                    <h1 class="entry-title center"><?php the_title(); ?></h1>
                </div>
                <?php $confirm_text = get_field('confirm_text');
                if (!empty($confirm_text)): ?>
                    <p><?= $confirm_text; ?></p>
                <?php else: ?>
                    <p>Ich melde mich unter
                        Anerkennung der "Allgemeinen Informationen" der JeKits-Stiftung verbindlich für diese Fortbildung an.</p>
                    <p>Bitte beachten Sie: Das Fortbildungsangebot richtet sich an Lehrkräfte aus NRW, die bei einem am JeKits-Programm beteiligten außerschulischen Bildungspartner oder einer beteiligten Grund- oder Förderschule beschäftigt sind. Auch externe Lehrkräfte können an den Fortbildungen der Stiftung teilnehmen, wenn nach Anmeldeschluss noch Plätze frei sind. Für interessierte externe Lehrkräfte steht ein gesondertes Anmeldeformular innerhalb der Online-Anmeldung zur Verfügung.</p>
                <?php endif; ?>
                <p><i>Die mit * gekennzeichneten Felder sind Pflichtfelder.</i></p>
                <br/><br/>
                <p>
                    <?php if (!empty(get_field('checkbox_confirm_label'))): ?>
                        <?= get_field('checkbox_confirm_label'); ?>
                    <?php else: ?>
                        Sind Sie JeKits-Lehrkraft in Nordrhein-Westfalen?
                    <?php endif; ?>
                    <input style="margin: 0 5px 0 20px;" id="agree_yes" type="radio" name="agree[]" value="yes"/><label for="agree_yes">Ja</label>
                    <input style="margin: 0 5px 0 20px;" id="agree_no" type="radio" name="agree[]" value="no"/><label for="agree_no">Nein</label>
                <div id="alert" class="alert-danger" style="display: none;">Bitte auswählen</div>
                </p>
                <br/><br/><br/>
                <?php
                $form_page_id = get_field('form_page_id');
                if (empty($form_page_id)) {
                    $form_page_id = get_field('form_page_id', 'options');
                }

                if ($form_page_id):
                    ?>
                    <div class="button-wrapper">
                        <?php

                        // this section is a (inherited) mess in its approach and style

                        $current_post = get_post();

                        $page_id_external = get_field('page_id_download', 'options'); // why is it named "download"?

                        $course_params = '?kurs_id=' . urlencode(get_the_title());

                        if ($current_post->post_excerpt) {
                            $course_params .= '&kurs_name=' . urlencode($current_post->post_excerpt); // why the fucking excerpt?!
                        }

                        $form_url = get_permalink($form_page_id) . $course_params;
                        $external_url = get_permalink($page_id_external) . $course_params;

                        ?>
                        <a id="agree" class="button" href="#"><?php echo __('Weiter zur Online-Anmeldung', 'roots'); ?></a>
                        <script>
                            var agree_links = {
                                'form': '<?php echo $form_url ?>',
                                'external': "<?php echo $external_url; ?>"
                            }
                        </script>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
