<?php
/*
Template Name: FAQ
*/
?>
<section class="content-header" style="background-color: #03256a;">
    <div class="container">
        <div class="content-intro">
            <h1>
                <?php echo roots_title(); ?>
            </h1>
            <p class="description light">Hier finden Sie Antworten auf die am häufigsten gestellten Fragen. Bitte wählen Sie einen Themenbereich aus. Anschließend werden Ihnen die zugehörigen Fragen inklusive Antwort angezeigt.</p>
        </div>
    </div>
</section>
<section class="main-content team">
    <div class="container">
        <div class="content-wrap">
            <div class="content-box">
                <?php
                $faq_sections = get_field('sections');
                foreach ($faq_sections as $section):?>
                    <h2 style=""><?php echo $section['title']?></h2>
                    <?php
                    $faqs = $section['faq'];
                    if ($faqs): ?>
                        <div class="content-accordion" style="margin-top:10px;margin-bottom:50px;">
                            <?php foreach ($faqs as $key => $faq):
                                ?>
                                <article class="accordion" id="faq-accordion">
                                    <header>
                                        <h3 class="entry-title"><a href="#"><?php echo $faq['question']; ?></a></h3>
                                    </header>
                                    <div class="entry">
                                        <?php echo $faq['answer']; ?>
                                    </div>
                                </article>
                            <?php endforeach; ?>
                        </div>
                    <?php
                    endif;
                endforeach;
                ?>
            </div>
        </div>
    </div>
</section>