<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<?php get_template_part('templates/content', 'single'); ?>
			</div>
		</div>
	</div>
</section>