<?php
/*
Template Name: Blog
*/

$blog_terms = get_field('options_blog_category', 'option');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'paged'          => $paged,
    'posts_per_page' => -1,
    'tax_query'      => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'ID',
            'terms'    => $blog_terms,
        ),
    ),
);

$posts = new WP_Query($args);
$posts = $posts->get_posts();
?>
<?php get_template_part('templates/page', 'header'); ?>
<section class="main-content">
    <div class="container">
        <div class="row blog-posts">
            <!--            <div class="col-xs-12 col-sm-8 blog-posts">-->
            <?php foreach ($posts as $blog_post): ?>
                <div class="blog-post-item col-xs-12 col-sm-6">
                    <div class="inner">
                        <div class="row">
                            <div class="col-xs-5 blog-image">
                                <a href="<?php echo get_permalink($blog_post) ?>" style="background-image:url('<?php echo get_the_post_thumbnail_url($blog_post, 'blog_archive_entry'); ?>');"> </a>
                            </div>
                            <div class="col-xs-7 blog-text">
                                <div class="inner">
                                    <p class="meta date small"><?php echo get_the_date('d.m.Y', $post);?></p>
                                    <h2><a href="<?php echo get_permalink($blog_post) ?>"><?php echo $blog_post->post_title ?></a></h2>
                                    <p><?php echo get_the_excerpt($blog_post) ?></p>
                                    <p><a href="<?php echo get_permalink($blog_post) ?>" class="btn">Weiterlesen</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <!--            </div>-->
            <!--            <div class="col-xs-12 col-sm-4 sidebar">-->
            <!--                --><?php //dynamic_sidebar('sidebar-primary'); ?>
            <!--            </div>-->
        </div>
    </div>
</section>
