<?php while (have_posts()) : the_post();if(get_the_content()): ?>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
                    <?php //get_template_part('templates/page', 'header'); ?>
					<?php get_template_part('templates/content', 'page'); ?>
					<?php get_template_part('templates/accordion'); ?>
			</div>
		</div>
	</div>
</section>
<?php endif;endwhile;?>
