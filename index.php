<?php get_template_part('templates/page', 'header'); ?>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<?php if (!have_posts()) : ?>
				  <div class="alert alert-warning">
				    Keine Beiträge gefunden
				  </div>
				  <?php get_search_form(); ?>
				<?php endif; ?>

				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('templates/content', get_post_format()); ?>
				<?php endwhile; ?>
				<?php if (1==2 && $wp_query->max_num_pages > 1) : ?>
					<nav class="post-nav">
						<ul class="pager">
							<li class="previous"><?php next_posts_link(__('&larr; Ältere Beiträge', 'roots')); ?></li>
							<li class="next"><?php previous_posts_link(__('Neuere Beiträge &rarr;', 'roots')); ?></li>
						</ul>
					</nav>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>