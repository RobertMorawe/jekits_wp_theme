<?php
/*
Template Name: Single-Page
*/
?>
<section class="main-content">
	<div class="container">
		<div class="content-wrap">
			<div class="content-box">
				<?php while (have_posts()) : the_post(); ?>
					<?php $show_title = get_field('hide_title'); ?>
					<?php if(isset($show_title) && $show_title != true ): ?>
						<?php get_template_part('templates/page', 'header'); ?>
					<?php endif; ?>
  				<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<?php include('templates/single-query.php'); ?>